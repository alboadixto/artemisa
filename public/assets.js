const CSS = [
    "admin-lte/plugins/fontawesome-free",
    "admin-lte/dist/css/adminlte.min.css",
    "admin-lte/dist/css/adminlte.min.css.map",
    "bootstrap-fileinput/css/fileinput.min.css",
    "datatables.net-bs4/css/dataTables.bootstrap4.min.css",
    "alertifyjs/build/css/alertify.min.css",
    "alertifyjs/build/css/themes/bootstrap.min.css",
    "admin-lte/plugins/daterangepicker/daterangepicker.css",
];

const JS = [
    "admin-lte/plugins/jquery/jquery.min.js",
    "admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js",
    "admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js.map",
    "admin-lte/dist/js/adminlte.min.js",
    "admin-lte/dist/js/adminlte.min.js.map",
    "admin-lte/dist/js/demo.js",
    "admin-lte/plugins/jquery-validation/additional-methods.min.js",
    "admin-lte/plugins/jquery-validation/jquery.validate.min.js",
    "admin-lte/plugins/jquery-validation/localization/messages_es.min.js",
    "bootstrap-fileinput/js/fileinput.min.js",
    "bootstrap-fileinput/js/plugins/piexif.min.js",
    "bootstrap-fileinput/js/plugins/purify.min.js",
    "bootstrap-fileinput/js/plugins/sortable.min.js",
    "bootstrap-fileinput/js/locales/es.js",
    "bootstrap-fileinput/themes/fas/theme.min.js",
    "datatables.net/js/jquery.dataTables.js",
    "datatables.net-bs4/js/dataTables.bootstrap4.min.js",
    "alertifyjs/build/alertify.min.js",
    "admin-lte/plugins/daterangepicker/daterangepicker.js",
    "admin-lte/plugins/moment/moment.min.js",
];

module.exports = [...CSS, ...JS];
