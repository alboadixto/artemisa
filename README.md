## Install Artemisa

Para instalar el sistema **Artemisa** se deben seguir los siguientes pasos:

Abrir la terminal y ejecutar:

1. git clone https://gitlab.com/alboadixto/artemisa.git
2. git pull origin la-ultima-milla
3. git checkout la-ultima-milla
4. composer install
5. configurar archivo .env (no olvidar ingresar credenciales de **[pusher](https://dashboard.pusher.com/)**)
6. php artisan migrate --path=/database/migrations/2021_11_22_102706_create_notifications_table.php
7. php artisan key:generate

## Credenciales

URL DEV: http://dev.laultimamilla.cl/ <br />
Usuario: admin@gmail.com <br />
Contraseña: 123 <br />
Contraseña encriptada DB: $2y$10$XQYIxDNBXdgQPJr.wht66OSchH4Eol8vi7kxpq19P8JyiG.wUmX0y

### Pusher channel artemisa dev:

BROADCAST_DRIVER=pusher

PUSHER_APP_ID=1310795 <br />
PUSHER_APP_KEY=0937d57580d9a908a4ba <br />
PUSHER_APP_SECRET=d62e509d0298bfaf71d2 <br />
PUSHER_APP_CLUSTER=us2
