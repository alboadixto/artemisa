const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");
const Assets = require("./public/assets");
module.exports = {
    entry: "./public/app.js",
    output: {
        path: __dirname + "/dist",
        filename: "bundle.js"
    },
    plugins: [
        new CopyPlugin({
            patterns: Assets.map(asset => {
                return {
                    from: path.resolve(__dirname, `./node_modules/${asset}`),
                    to: path.resolve(__dirname, "public/libs")
                };
            })
        })
    ]
};