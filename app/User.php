<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin\Rol;
use Illuminate\Support\Facades\Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'canal_venta'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        // Nos relacionamos muchos a muchos con la tabla rol a traves de la tabla union_rol_users
        return $this->belongsToMany(Rol::class, 'union_rol_users');
    }

    public function setSession($roles)
    {
        //dd($roles[0]['nombre']);
        Session::put([
            'usuario_email'       => $this->email,
            'usuario_id'          => $this->id,
            'usuario_nombre'      => $this->name,
            'usuario_canal_venta' => $this->canal_venta
        ]);
        if (count($roles) > 0) {
            Session::put('roles', $roles);
        }
    }
}
