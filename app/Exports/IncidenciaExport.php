<?php

namespace App\Exports;

use App\Models\Incidencia;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;

class IncidenciaExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents,WithTitle
{

    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function title(): string
    {
        return 'Incidencia';
    }

    public function headings(): array
    {
        return [
            'Pedido Int.',
            'Pedido Ext.',
            'Comentarios',
            'Estado',
            'Prioridad',
            'Fecha',
            'Usuario'
        ];
    }

    public function collection()
    {
        /**------------------------------------------------------------------------
         *                           VARIABLES
         *------------------------------------------------------------------------**/
        $fecha_ini      = $this->request->fecha_ini;
        $fecha_fin      = $this->request->fecha_fin;
        $canal_venta    = $this->request->canal_venta;
        /*---------------------------- END OF VARIABLES ----------------------------*/

        //return Incidencia::all();
        $incidencia_export  = Incidencia::select('incidencia.referencia_pedido' //A - Red Pedido Int
                                                    , 'partida.pedido_externo' //B - Ref Pedido Ext
                                                    , 'incidencia.comentario' //C - Comentarios
                                                    , 'estado.estado_descripcion' //D - Estado
                                                    , 'prioridad.prioridad_descripcion' //E - Prioridad
                                                    , DB::raw('DATE_FORMAT(`incidencia`.`created_at`, "%Y-%m-%d %H:%i:%s")') //F - Fecha
                                                    , 'users.name') //G - Usuario
                                            ->join('partida', 'partida.referencia_pedido', 'incidencia.referencia_pedido')
                                            ->join('estado', 'estado.id', 'incidencia.id_estado')
                                            ->join('users', 'users.id', 'incidencia.user_id')
                                            ->join('prioridad', 'prioridad.id', 'incidencia.id_prioridad')
                                            ->whereBetween('incidencia.created_at', [$fecha_ini, $fecha_fin])
                                            ->when($canal_venta, function ($query, $canal_venta) {
                                                    return $query->where('partida.canal_venta', $canal_venta);
                                            })
                                            ->groupBy('incidencia.referencia_pedido'
                                                        , 'partida.pedido_externo'
                                                        , 'incidencia.comentario'
                                                        , 'estado.estado_descripcion'
                                                        , 'prioridad.prioridad_descripcion'
                                                        , 'incidencia.created_at'
                                                        , 'users.name')
                                            ->orderBy('incidencia.created_at', 'desc')
                                            ->get();
                                            //->toSql();
                                            //dd($incidencia_export);
        return $incidencia_export;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:G1'; // All headers
                $style = [
                    //https://www.programmersought.com/article/20812205559/
                    'font' => [
                        'name' => 'Arial',
                        'bold' => true,
                        'italic' => false,
                        //'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLE,
                        'strikethrough' => false,
                        'color' => ['rgb' => 'ffffff']
                    ],
                    /* 'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                    ], */
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => ['argb' => 'FF4F81BD']
                    ]
                ];
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style)->getFont()->setSize(14);
                //$event->sheet->getStyle($cellRange)->applyFromArray($styleBorder);
            },
        ];
    }
}
