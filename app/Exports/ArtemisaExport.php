<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ArtemisaExport implements WithMultipleSheets
{

    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function sheets(): array
    {
        return [
            'IncidenciaExport'  => new IncidenciaExport($this->request),
            'SlaExport'         => new SlaExport($this->request),
        ];
    }

}
