<?php

namespace App\Exports;

use App\Models\Partida;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;

class SlaExport implements FromCollection,WithHeadings,ShouldAutoSize,WithEvents,WithTitle
{

    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function title(): string
    {
        return 'SLA';
    }

    public function headings(): array
    {
        return [
            'SLA',
            'Región',
            'Canal Venta',
            'Pedido Int.'
        ];
    }

    public function collection()
    {

        /**------------------------------------------------------------------------
         *                           VARIABLES
         *------------------------------------------------------------------------**/
        $fecha_ini      = $this->request->fecha_ini;
        $fecha_fin      = $this->request->fecha_fin;
        $canal_venta    = $this->request->canal_venta;
        /*---------------------------- END OF VARIABLES ----------------------------*/

        $sla_export = Partida::selectRaw("(SELECT
                                            (TIMEDIFF((SELECT
                                                                MAX(i.created_at)
                                                            FROM
                                                                incidencia i
                                                            WHERE
                                                                i.referencia_pedido = partida.referencia_pedido),
                                                        (SELECT
                                                                MIN(i.created_at)
                                                            FROM
                                                                incidencia i
                                                            WHERE
                                                                i.referencia_pedido = partida.referencia_pedido)))
                                            ) AS SLA,
                                            (SELECT
                                                    region_nombre
                                                FROM
                                                    comunas
                                                        INNER JOIN
                                                    provincias ON comunas.provincia_id = provincias.provincia_id
                                                        INNER JOIN
                                                    regiones ON provincias.region_id = regiones.region_id
                                                WHERE
                                                    comunas.comuna_nombre LIKE partida.dir_entrega) AS region_nombre,
                                            partida.canal_venta,
                                            partida.referencia_pedido")
                                ->join(DB::raw('(select
                                                    i.referencia_pedido,
                                                    i.created_at,
                                                    i.caso_cerrado,
                                                    i.id_estado
                                                from incidencia i
                                                inner join
                                                    (
                                                        select
                                                            i.referencia_pedido,
                                                            max(i.created_at) fecha
                                                        from incidencia i
                                                        where i.created_at BETWEEN "'.$fecha_ini.'" and "'.$fecha_fin.'"
                                                        group by i.referencia_pedido) i2
                                                on (i.created_at = i2.fecha AND i.caso_cerrado = 1)) i'), 'partida.referencia_pedido', 'i.referencia_pedido')
                                ->when($canal_venta, function ($query, $canal_venta) {
                                        return $query->where('partida.canal_venta', $canal_venta);
                                })
                                ->groupBy('region_nombre'
                                        , 'partida.canal_venta'
                                        , 'partida.referencia_pedido')
                                ->get();
                                //->toSql();
                                //dd($sla_export);

        return $sla_export;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:D1'; // All headers
                $style = [
                    //https://www.programmersought.com/article/20812205559/
                    'font' => [
                        'name' => 'Arial',
                        'bold' => true,
                        'italic' => false,
                        //'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLE,
                        'strikethrough' => false,
                        'color' => ['rgb' => 'ffffff']
                    ],
                    /* 'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                    ], */
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => ['argb' => 'FF4F81BD']
                    ]
                ];
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style)->getFont()->setSize(14);
                //$event->sheet->getStyle($cellRange)->applyFromArray($styleBorder);
            },
        ];
    }

}
