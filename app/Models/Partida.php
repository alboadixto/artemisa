<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    protected $table    = "partida";
    protected $fillable = ['canal_venta', 'sitio_origen', 'referencia_pedido', 'pedido_externo', 'fecha_creacion', 'nombre_mostrado', 'cantidad', 'precio', 'dir_entrega', 'nombre_cliente', 'rut_cliente'];
    protected $guarded  = ['id'];
}
