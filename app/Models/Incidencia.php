<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incidencia extends Model
{
    protected $table    = "incidencia";
    protected $fillable = ['referencia_pedido', 'comentario', 'id_estado', 'user_id', 'id_prioridad', 'caso_cerrado'];
    protected $guarded  = ['id'];
}
