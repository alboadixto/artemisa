<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table    = "estado";
    protected $fillable = ['estado_descripcion', 'is_active'];
    protected $guarded  = ['id'];
    public $timestamps = false;
}
