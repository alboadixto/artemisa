<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prioridad extends Model
{
    protected $table    = "prioridad";
    protected $fillable = ['prioridad_descripcion', 'is_active', 'color'];
    protected $guarded  = ['id'];
}
