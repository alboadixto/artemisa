<?php

namespace App\Imports;

use App\Models\Partida;
use Maatwebsite\Excel\Concerns\ToModel;

class PartidasImport implements ToModel
{
    private $rows = 0;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $cantidad = ++$this->rows;
        //se salta la primera linea ya que en el excel viene el titulo primero
        if($cantidad > 1){
            //comprobar columna A del excel viene con algun dato
            if($row[0]){
                //busco si el registro del excel se encuentra en la tabla partida
                $encontrado = Partida::where('referencia_pedido', $row[2]) //PV3243
                                        ->where('nombre_mostrado', $row[5]) //PV3243 - [LUM00465] Choclo 200 gr
                                        ->first();
                //si no encuentra el registro lo inserta en la tabla partida
                if(!$encontrado){
                    return new Partida([
                            'canal_venta'       => $row[0], // A -> Kimberly-Clark
                            'sitio_origen'      => $row[1], // B -> Mercado Libre
                            'referencia_pedido' => $row[2], // C -> PV3249
                            'pedido_externo'    => $row[3], // D -> 2585797162
                            //'fecha_creacion'    => $row[4], // E ->
                            'nombre_mostrado'   => $row[5], // F -> PV3243 - [LUM00465] Choclo 200 gr
                            'cantidad'          => $row[6], // G -> 2
                            'precio'            => $row[7], // H -> 990
                            'dir_entrega'       => $row[8], // I -> Lo Barnechea
                            'nombre_cliente'    => $row[9], // J -> Juan Pablo Meza
                            'rut_cliente'       => $row[10], // K -> 13026238-4
                    ]);
                }
            }
        }
    }

    // cantidad de datos importados que vienen en el excel
    public function getRowCount(): int
    {
        return $this->rows;
    }
}
