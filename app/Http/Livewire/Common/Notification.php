<?php

namespace App\Http\Livewire\Common;

use Carbon\Carbon;
use Livewire\Component;

class Notification extends Component
{
    public $count_unread;
    public $count_read;
    public $unreadNotifications;
    public $readNotifications;

    protected $listeners = ['mensajeRecibido'];

    public function mount()
    {
        $this->unreadNotifications = auth()->user()->unreadNotifications;
        $this->readNotifications = auth()->user()->readNotifications->where('created_at', '>=', Carbon::now()->subDays(10)->toDateTimeString());

        $this->count_unread = count($this->unreadNotifications);
        $this->count_read = count($this->readNotifications);
    }

    public function  mensajeRecibido($data)
    {
        $this->actualizarMensajes();
    }

    public function actualizarMensajes()
    {
        $this->unreadNotifications = auth()->user()->unreadNotifications;
        $this->readNotifications = auth()->user()->readNotifications->where('created_at', '>=', Carbon::now()->subDays(10)->toDateTimeString());

        $this->count_unread = count($this->unreadNotifications);
        $this->count_read = count($this->readNotifications);
    }

    public function markNotification($id)
    {
        $this->unreadNotifications->where('id', $id)->markAsRead();
        $this->actualizarMensajes();
    }

    public function render()
    {
        return view('livewire.common.notification');
    }
}
