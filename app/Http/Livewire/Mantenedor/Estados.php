<?php

namespace App\Http\Livewire\Mantenedor;

use App\Models\Estado;
use Livewire\Component;

class Estados extends Component
{
    public $estados, $estado_descripcion, $is_active, $estado_id;
    public $formulario = false;
    protected $listeners = ['delete'];

    public function render()
    {
        $this->estados = Estado::where('is_active', '<>', 2)->get();

        return view('livewire.mantenedor.estado.table');
    }

    public function crear()
    {
        $this->limpiarCampos();
        $this->abrirFormulario();
    }

    public function abrirFormulario()
    {
        $this->formulario = true;
    }

    public function cerrarFormulario()
    {
        $this->formulario = false;
    }

    public function limpiarCampos()
    {
        $this->estado_descripcion = '';
        $this->is_active = '';
        $this->estado_id = '';
    }

    public function edit($id)
    {
        $estado = Estado::findOrFail($id);
        $this->estado_id = $id;
        $this->estado_descripcion = $estado->estado_descripcion;
        $this->is_active = $estado->is_active; //se envia a la vista create.blade.php en el select con el valor 0=No o 1=Si

        $this->abrirFormulario();
    }

    public function store()
    {
        Estado::updateOrCreate(
            ['id' => $this->estado_id],
            [
                'estado_descripcion' => $this->estado_descripcion,
                'is_active' => (strlen($this->is_active)) ? $this->is_active : 1
            ]
        );

        $this->cerrarFormulario();
        $this->limpiarCampos();

        $this->emit('alert', 'El registro se guardó correctamente.');
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('alert:confirm', [
            'title' => '<i class="icon fas fa-exclamation-triangle"></i> Importante!',
            'message' => 'Está seguro que desea eliminar el registro?',
            'id' => $id
        ]);
    }

    public function delete($id)
    {
        Estado::find($id)->delete();
    }
}
