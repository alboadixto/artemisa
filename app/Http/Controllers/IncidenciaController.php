<?php

namespace App\Http\Controllers;

use App\Events\IncidenciaEvent;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ArtemisaExport;
use App\Models\Partida;
use App\Models\Incidencia;
use App\Models\Estado;
use App\Models\Prioridad;
use App\User;
use App\Notifications\IncidenciaNotification;
use Illuminate\Support\Facades\Auth;

class IncidenciaController extends Controller
{

    public function index(Request $request)
    {

        //$roles = session()->get('roles'); // admin

        /**------------------------------------------------------------------------
         *                           VARIABLES
         *------------------------------------------------------------------------**/
        //* usuario_canal_venta se setea en app/User.php
        $canal_venta    = session()->get('usuario_canal_venta'); // Marley Coffee

        $fecha_ini      = date('Y-m-01') . " 00:00:00"; //Día actual
        $fecha_fin      = date('Y-m-d') . " 23:59:59"; //Día actual

        if ($request->daterange_filter) {
            $fecha_rango    = explode(" | ", $request->daterange_filter);
            $fecha_ini      = $fecha_rango[0] . " 00:00:00";
            $fecha_fin      = $fecha_rango[1] . " 23:59:59";
        }

        //* obtener las empresas
        $empresas   = Partida::groupBy('canal_venta')
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('partida.canal_venta', $canal_venta);
            })
            ->orderBy('canal_venta', 'asc')
            ->pluck('canal_venta');
        /*---------------------------- END OF VARIABLES ----------------------------*/

        return view('incidencia.index', compact(
            'canal_venta',
            'fecha_ini',
            'fecha_fin',
            'empresas'
        ));
    }


    public function mostrarComentario(Request $request)
    {
        $referencia_pedido                = $request->referencia_pedido; //PV3243
        $pedido_externo                   = $request->pedido_externo;    //ariztia000000086
        $cliente_nombre                   = $request->cliente_nombre;    // Fabiola Sepúlveda Avalos
        $cliente_rut                      = $request->cliente_rut;       //15028017-6

        $productos                        = Partida::where('referencia_pedido', $referencia_pedido)
            ->get(); //busca todos los productos que tengan esa referencia

        $historial_comentario             = Incidencia::select(
            'incidencia.comentario',
            'incidencia.caso_cerrado',
            'estado.estado_descripcion',
            'prioridad.prioridad_descripcion',
            'prioridad.color as color_prioridad',
            'incidencia.created_at',
            'users.name'
        )
            ->where('referencia_pedido', $referencia_pedido)
            ->join('estado', 'estado.id', 'incidencia.id_estado')
            ->join('users', 'users.id', 'incidencia.user_id')
            ->join('prioridad', 'prioridad.id', 'incidencia.id_prioridad')
            ->orderBy('created_at', 'desc')
            ->get();

        $estados                          = Estado::where('is_active', 1)
            ->orderBy('estado_descripcion')
            ->get();

        $prioridades                      = Prioridad::where('is_active', 1)
            ->orderBy('id')
            ->get();

        //variables tabla productos
        $info_productos         = "";
        $indice_productos       = 0;
        $cantidad_total         = 0;
        $precio_total           = 0;
        foreach ($productos as $value) {
            $indice_productos++;
            $cantidad_total += $value->cantidad;
            $precio_total += $value->precio;
            $info_productos .= '<tr>
                                    <td>' . $indice_productos . '</td>
                                    <td>' . $value->nombre_mostrado . '</td>
                                    <td align="center">' . $value->cantidad . '</td>
                                    <td align="right">$' . number_format($value->precio, 0, '', '.') . '</td>
                                </tr>';
        }

        //variables tabla historial comentario
        $info_historial_comentario = "";
        foreach ($historial_comentario as $value) {
            $color_tr = '';
            if ($value->caso_cerrado == 1) {
                $color_tr = 'class="table-danger"';
            }
            $info_historial_comentario .= '<tr ' . $color_tr . '>
                                              <td>' . $value->comentario . '</td>
                                              <td>' . $value->estado_descripcion . '</td>
                                              <td class="text-center"><span class="badge bg-' . $value->color_prioridad . '">' . $value->prioridad_descripcion . '</span></td>
                                              <td>' . $value->created_at . '</td>
                                              <td>' . $value->name . '</td>
                                          </tr>';
        }

        $info_select_estado = "";
        foreach ($estados as $estado) {
            $info_select_estado .= '<option value="' . $estado->id . '">' . $estado->estado_descripcion . '</option>';
        }

        $info_select_prioridad = "";
        foreach ($prioridades as $prioridad) {
            $info_select_prioridad .= '<option value="' . $prioridad->id . '">' . $prioridad->prioridad_descripcion . '</option>';
        }

        $data = '
                <a href="javascript:;" class="btn btn-info btn-sm" title="Volver" onclick="changeVisibility()">
                    <i class="fas fa-arrow-circle-left"></i>
                </a>
                <br><br>

                <!-- tabla productos -->
                <div class="card">
                    <div class="overlay">
                        <i class="fas fa-3x fa-sync-alt fa-spin"></i>
                        <div class="text-bold pt-2">Cargando...</div>
                    </div>

                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-s text-nowrap">
                            <thead>
                                <tr>
                                    <th width="10px">#</th>
                                    <th>Nombre</th>
                                    <th width="10px">Cantidad</th>
                                    <th width="10px">Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                ' . $info_productos . '
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2" align="right">Total</td>
                                    <td align="center">' . $cantidad_total . '</td>
                                    <td>$' . number_format($precio_total, 0, '', '.') . '</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>

                <!-- /formulario comentarios -->
                <div class="card">
                    <form role="form" class="form-horizontal" id="form-comentario-incidencia">
                        <div class="card-body">
                            <input type="hidden" id="referencia_pedido" name="referencia_pedido" value="' . $referencia_pedido . '">
                            <input type="hidden" id="pedido_externo" name="pedido_externo" value="' . $pedido_externo . '">
                            <div class="form-group row">
                                <label for="id_estado" class="col-sm-2 col-form-label">Estado</label>
                                <div class="col-sm-4">
                                    <select name="id_estado" id="id_estado" class="form-control">
                                        ' . $info_select_estado . '
                                    </select>
                                </div>

                                <label for="id_prioridad" class="col-sm-2 col-form-label">Prioridad</label>
                                <div class="col-sm-4">
                                    <select name="id_prioridad" id="id_prioridad" class="form-control">
                                        ' . $info_select_prioridad . '
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="comentario" class="col-sm-2 col-form-label">Comentario</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" id="comentario" name="comentario" placeholder="Comentario"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="button" class="btn btn-info float-right btn_guardar" onclick="btn_guardar_comentario_incidencia()">Guardar</button>
                            <button type="button" class="btn btn-danger btn_cerrar_caso" onclick="btn_cerrar_caso_comentario_incidencia()">Cerrar Caso</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>

                <!-- /tabla historial comentarios -->
                <div class="card" style="display: none;">
                    <div class="overlay">
                        <i class="fas fa-3x fa-sync-alt fa-spin"></i>
                        <div class="text-bold pt-2">Cargando...</div>
                    </div>

                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-s">
                            <thead>
                                <tr>
                                    <th width="700px">Historial Comentario</th>
                                    <th>Estado</th>
                                    <th>Prioridad</th>
                                    <th>Fecha</th>
                                    <th>Usuario</th>
                                </tr>
                            </thead>
                            <tbody>
                                ' . $info_historial_comentario . '
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
        ';

        return $data;
    }


    public function guardarComentario(Request $request)
    {
        $incidencia = Incidencia::create([
            'referencia_pedido' => $request->form["referencia_pedido"],
            'comentario'        => $request->form["comentario"],
            'id_estado'         => $request->form["id_estado"], //3
            'user_id'           => session()->get('usuario_id'), //1
            'id_prioridad'      => $request->form["id_prioridad"], //1
            'caso_cerrado'      => $request->form["caso_cerrado"] //0
        ]);

        if ($incidencia) {
            $canal_venta_user = session()->get('usuario_canal_venta'); // Marley Coffee
            $canal_venta_notification = Partida::select('canal_venta')
                ->where('referencia_pedido', $request->form["referencia_pedido"])
                ->first();

            /*
            User::all()
                ->where('canal_venta', $canal_venta["canal_venta"])
                ->each(function (User $user) use ($incidencia) {
                    $user->notify(new IncidenciaNotification($incidencia));
                });
            Auth()->user()->notify(new IncidenciaNotification($incidencia));
            */

            //enviar la notificación push solo si existen hasta 2 incidencias.
            $incidenciaCount = Incidencia::where('referencia_pedido', $request->form["referencia_pedido"])->count();

            if ($incidenciaCount <= 2) {
                if ($canal_venta_user == $canal_venta_notification["canal_venta"]) {
                    $canal_venta_notification["canal_venta"] = 'Administrador';
                }

                //registra en la tabla notifications
                event(new IncidenciaEvent($incidencia, $canal_venta_notification["canal_venta"]));
            }

            return response()->json(['mensaje' => 'ok']);
        } else {
            return response()->json(['mensaje' => 'ng']);
        }
    }


    public function exportarIncidencia(Request $request)
    {

        $export = new ArtemisaExport($request);
        return Excel::download($export, 'incidencias.xlsx');
    }
}
