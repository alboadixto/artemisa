<?php

namespace App\Http\Controllers;

use App\Models\Estado as ModelsEstado;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    public function index()
    {
        $estados = ModelsEstado::where('is_active', '<>', 2)->get();
        return view('mantenedor.estado.index')->with('estados', $estados);
    }

    public function create()
    {
        return view('mantenedor.estado.create');
    }

    public function store(Request $request)
    {
        $estado = new ModelsEstado();
        $estado->estado_descripcion = $request->get('descripcion');
        $estado->save();

        return redirect('estados/index');
    }

    public function edit($id)
    {
        $estado = ModelsEstado::find($id);

        return view('mantenedor.estado.edit')->with('estado', $estado);
    }

    public function update(Request $request, $id)
    {
        $estado = ModelsEstado::find($id);
        $estado->estado_descripcion = $request->get('descripcion');
        $estado->is_active = $request->get('is_active');
        $estado->save();

        return redirect('estados/index');
    }

    public function destroy($id)
    {

        $estado = ModelsEstado::find($id);
        $estado->delete();

        return redirect()->route('estado.index');
    }
}
