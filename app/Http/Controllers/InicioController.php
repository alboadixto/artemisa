<?php

namespace App\Http\Controllers;

use App\Models\Incidencia;
use Illuminate\Http\Request;
use App\Models\Estado;
use App\Models\Partida;
use App\Models\Prioridad;
use DB;
use DateTime;

class InicioController extends Controller
{

    public function index(Request $request)
    {

        //dd(session()->all());

        /**------------------------------------------------------------------------
         *                           VARIABLES
         *------------------------------------------------------------------------**/
        //* usuario_canal_venta se setea en app/User.php
        $user_canal_venta   = session()->get('usuario_canal_venta'); // Marley Coffee
        $canal_venta        = $user_canal_venta;

        $fecha_ini   = date('Y-m-01') . " 00:00:00"; //Día 1 del mes actual
        $fecha_fin   = date('Y-m-d') . " 23:59:59";  //Día actual
        $anho_actual = date('Y');

        if ($request->daterange_filter) {
            $fecha_rango    = explode(" | ", $request->daterange_filter);
            $fecha_ini      = $fecha_rango[0] . " 00:00:00";
            $fecha_fin      = $fecha_rango[1] . " 23:59:59";
        }

        if ($request->canal_venta && !$user_canal_venta) {
            $canal_venta = $request->canal_venta;
        }

        //* obtener las empresas
        $empresas   = Partida::groupBy('canal_venta')
            ->when($user_canal_venta, function ($query, $user_canal_venta) {
                return $query->where('partida.canal_venta', $user_canal_venta);
            })
            ->orderBy('canal_venta', 'asc')
            ->pluck('canal_venta');
        /*---------------------------- END OF VARIABLES ----------------------------*/



        /**------------------------------------------------------------------------
         *                           CONTADOR
         *------------------------------------------------------------------------**/
        $incidencias = Incidencia::select(
            'incidencia.referencia_pedido',
            'incidencia.created_at',
            'incidencia.caso_cerrado'
        )
            ->whereBetween('incidencia.created_at', [$fecha_ini, $fecha_fin])
            ->join(
                DB::raw('(select p.referencia_pedido, p.canal_venta
                    from partida p
                    group by p.referencia_pedido, p.canal_venta) partida'),
                'incidencia.referencia_pedido',
                'partida.referencia_pedido'
            )
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('partida.canal_venta', $canal_venta);
            })
            ->orderBy('incidencia.referencia_pedido', 'desc')
            ->orderBy('incidencia.id', 'desc')
            ->get();

        $cont = 0;
        $referencia_pedido = "";
        $fecha_max = "";
        $count_desarrollo = 0;
        $count_caso_cerrado = 0;
        $count_demorado = 0;
        $count_todos = 0;

        foreach ($incidencias as $incidencia) {
            if ($referencia_pedido != $incidencia->referencia_pedido) {
                $flag_caso_cerrado = 0;
                $cont = 0;
            }

            if ($cont == 0) {
                $referencia_pedido = $incidencia->referencia_pedido;
                $fecha_max = $incidencia->created_at;

                if ($incidencia->caso_cerrado == 1) {
                    $flag_caso_cerrado = 1;
                    $count_caso_cerrado++;
                } else {
                    $count_desarrollo++;
                }

                $cont++;
            } else {
                if ($cont == 1 && $flag_caso_cerrado == 0) {
                    $fecha_anterior = new DateTime($incidencia->created_at);
                    $fecha_max = new DateTime($fecha_max);
                    $interval = $fecha_anterior->diff($fecha_max);

                    if (($interval->days * 24 + (int)$interval->format('%H')) > 23) {
                        $count_demorado++;
                    }

                    $cont++;
                }
            }
        }

        $count_todos = ($count_caso_cerrado + $count_desarrollo);
        /*---------------------------- END OF CONTADOR ----------------------------*/



        /**------------------------------------------------------------------------
         *                           TOP 3 INCIDENCIAS
         *------------------------------------------------------------------------**/
        $top_incidencias    = Incidencia::select(
            DB::raw(
                'count(estado.estado_descripcion) as cantidad
                                                    , sum(
                                                        (select sum(total) as precio
                                                            from (
                                                                select (precio*cantidad) as total, referencia_pedido
                                                                from partida ) p
                                                            where p.referencia_pedido = incidencia.referencia_pedido
                                                        )
                                                    ) as precio'
            ),
            'estado.estado_descripcion'
        )
            ->join(DB::raw('(select i.referencia_pedido,
                                                    max(i.created_at) fecha_creacion
                                                    from incidencia i
                                                    where i.caso_cerrado = 0
                                                    group by i.referencia_pedido) incidencia2'), 'incidencia.created_at', 'incidencia2.fecha_creacion')
            ->join('estado', 'incidencia.id_estado', 'estado.id')
            ->whereBetween('incidencia.created_at', [$fecha_ini, $fecha_fin])
            ->join(
                DB::raw('(select p.referencia_pedido, p.canal_venta
                                                                    from partida p
                                                                    group by p.referencia_pedido, p.canal_venta) partida'),
                'incidencia.referencia_pedido',
                'partida.referencia_pedido'
            )
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('partida.canal_venta', $canal_venta);
            })
            ->groupBy('estado.estado_descripcion')
            ->orderBy('precio', 'desc')
            ->orderBy('cantidad', 'desc')
            ->take(3)
            ->get();
        /*---------------------------- END OF TOP 3 INCIDENCIAS ----------------------------*/



        /**------------------------------------------------------------------------
         *                           GRÁFICOS
         *------------------------------------------------------------------------**/
        $chart_estados              = Incidencia::select(
            DB::raw('count(estado.estado_descripcion) as cantidad'),
            'estado.estado_descripcion'
        )
            ->join(DB::raw('(select i.referencia_pedido,
                                                                    max(i.created_at) fecha_creacion
                                                                    from incidencia i
                                                                    where i.caso_cerrado = 0
                                                                    group by i.referencia_pedido) incidencia2'), 'incidencia.created_at', 'incidencia2.fecha_creacion')
            ->join('estado', 'incidencia.id_estado', 'estado.id')
            ->whereBetween('incidencia.created_at', [$fecha_ini, $fecha_fin])
            ->join(
                DB::raw('(select p.referencia_pedido, p.canal_venta
                                                                    from partida p
                                                                    group by p.referencia_pedido, p.canal_venta) partida'),
                'incidencia.referencia_pedido',
                'partida.referencia_pedido'
            )
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('partida.canal_venta', $canal_venta);
            })
            ->groupBy('estado.estado_descripcion')
            ->orderBy('cantidad', 'desc')
            ->get();

        $chart_efectividad_total    = DB::table(DB::raw('(select count(p.referencia_pedido) as cantidad
                                                    , p.canal_venta
                                                    from partida p
                                                    where p.created_at BETWEEN "' . $fecha_ini . '" and "' . $fecha_fin . '"
                                                    group by p.referencia_pedido, p.canal_venta) p'))
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('p.canal_venta', $canal_venta);
            })
            ->count();

        $chart_efectividad_parcial  = DB::table(DB::raw('(select count(i.referencia_pedido) as cantidad
                                                    , i.referencia_pedido
                                                    from incidencia i
                                                    where i.created_at BETWEEN "' . $fecha_ini . '" and "' . $fecha_fin . '"
                                                    group by i.referencia_pedido) i'))
            ->join(
                DB::raw('(select p.referencia_pedido, p.canal_venta
                                                                    from partida p
                                                                    group by p.referencia_pedido, p.canal_venta) partida'),
                'i.referencia_pedido',
                'partida.referencia_pedido'
            )
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('partida.canal_venta', $canal_venta);
            })
            ->count();

        $chart_efectividad          = [
            'total'     => $chart_efectividad_total,
            'parcial'   => $chart_efectividad_parcial
        ];

        $chart_incidencia_vs_venta  = Partida::selectRaw("count(distinct(partida.referencia_pedido)) as venta
                                                        , CASE
                                                            WHEN max(MONTH(partida.created_at)) = 1 THEN 'Ene'
                                                            WHEN max(MONTH(partida.created_at)) = 2 THEN 'Feb'
                                                            WHEN max(MONTH(partida.created_at)) = 3 THEN 'Mar'
                                                            WHEN max(MONTH(partida.created_at)) = 4 THEN 'Abr'
                                                            WHEN max(MONTH(partida.created_at)) = 5 THEN 'May'
                                                            WHEN max(MONTH(partida.created_at)) = 6 THEN 'Jun'
                                                            WHEN max(MONTH(partida.created_at)) = 7 THEN 'Jul'
                                                            WHEN max(MONTH(partida.created_at)) = 8 THEN 'Ago'
                                                            WHEN max(MONTH(partida.created_at)) = 9 THEN 'Sep'
                                                            WHEN max(MONTH(partida.created_at)) = 10 THEN 'Oct'
                                                            WHEN max(MONTH(partida.created_at)) = 11 THEN 'Nov'
                                                            ELSE 'Dic'
                                                        END AS mes
                                                        , count(distinct(incidencia.referencia_pedido)) as incidencia")
            ->leftJoin('incidencia', 'partida.referencia_pedido', 'incidencia.referencia_pedido')
            ->whereYear('partida.created_at', $anho_actual)
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('partida.canal_venta', $canal_venta);
            })
            ->groupBy(DB::raw("MONTH(partida.created_at)"))
            ->orderBy(DB::raw("MONTH(partida.created_at)"))
            ->get();
        $chart_sla                  = Partida::selectRaw("(SELECT
                                                        (TIMEDIFF((SELECT
                                                                            MAX(i.created_at)
                                                                        FROM
                                                                            incidencia i
                                                                        WHERE
                                                                            i.referencia_pedido = partida.referencia_pedido),
                                                                    (SELECT
                                                                            MIN(i.created_at)
                                                                        FROM
                                                                            incidencia i
                                                                        WHERE
                                                                            i.referencia_pedido = partida.referencia_pedido)))
                                                        ) AS SLA,
                                                        (SELECT
                                                                region_nombre
                                                            FROM
                                                                comunas
                                                                    INNER JOIN
                                                                provincias ON comunas.provincia_id = provincias.provincia_id
                                                                    INNER JOIN
                                                                regiones ON provincias.region_id = regiones.region_id
                                                            WHERE
                                                                comunas.comuna_nombre LIKE partida.dir_entrega) AS region_nombre,
                                                        partida.canal_venta")
            ->join(DB::raw('(select i.referencia_pedido, i.created_at ,i.caso_cerrado, i.id_estado
                                                            from incidencia i
                                                            inner join
                                                                (
                                                                    select i.referencia_pedido,
                                                                    max(i.created_at) fecha
                                                                    from incidencia i
                                                                    where i.created_at BETWEEN "' . $fecha_ini . '" and "' . $fecha_fin . '"
                                                                    group by i.referencia_pedido) i2
                                                            on (i.created_at = i2.fecha AND i.caso_cerrado = 1)) i'), 'partida.referencia_pedido', 'i.referencia_pedido')
            ->when($canal_venta, function ($query, $canal_venta) {
                return $query->where('partida.canal_venta', $canal_venta);
            })
            ->groupBy(
                'region_nombre',
                'partida.canal_venta',
                'partida.referencia_pedido'
            )
            ->get();

        $cumplido_sla               = 0;
        $no_cumplido_sla            = 0;

        foreach ($chart_sla as $item) {
            if (intval($item->SLA) > 47 && $item->region_nombre == 'Metropolitana de Santiago') {

                $no_cumplido_sla++;
            } elseif (intval($item->SLA) > 119) {

                $no_cumplido_sla++;
            } else {

                $cumplido_sla++;
            }
        }

        $chart_cumplimiento_sla     = [
            'cumplido_sla'     => $cumplido_sla,
            'no_cumplido_sla'  => $no_cumplido_sla
        ];
        /*---------------------------- END OF GRÁFICOS ----------------------------*/


        return view('dashboard.inicio', compact(
            'user_canal_venta',
            'canal_venta',
            'empresas',
            'count_desarrollo',
            'count_caso_cerrado',
            'count_demorado',
            'count_todos',
            'chart_estados',
            'chart_efectividad',
            'chart_incidencia_vs_venta',
            'chart_cumplimiento_sla',
            'fecha_ini',
            'fecha_fin',
            'top_incidencias'
        ));
    }


    public function dashboardTimeline(Request $request)
    {
        /**------------------------------------------------------------------------
         *                           VARIABLES
         *------------------------------------------------------------------------**/
        $referencia_pedido                = $request->referencia_pedido; // PV3243
        $pedido_externo                   = $request->pedido_externo; // ariztia000000086
        $nombre_cliente                   = $request->nombre_cliente; // angelo carrasco
        $rut_cliente                      = $request->rut_cliente; // 17610656-5

        $productos                        = Partida::where('referencia_pedido', $referencia_pedido)
            ->get(); //busca todos los productos que tengan esa referencia

        $estados                          = Estado::where('is_active', 1)
            ->orderBy('estado_descripcion')
            ->pluck('estado_descripcion', 'id');

        $prioridades                      = Prioridad::where('is_active', 1)
            ->orderBy('id')
            ->pluck('prioridad_descripcion', 'id');
        /*---------------------------- END OF VARIABLES ----------------------------*/

        $timeline = Incidencia::select(
            'incidencia.comentario',
            'incidencia.caso_cerrado',
            'estado.estado_descripcion',
            'prioridad.prioridad_descripcion',
            'prioridad.color as color_prioridad',
            'incidencia.created_at',
            'users.name'
        )
            ->where('referencia_pedido', $referencia_pedido)
            ->join('estado', 'estado.id', 'incidencia.id_estado')
            ->join('users', 'users.id', 'incidencia.user_id')
            ->join('prioridad', 'prioridad.id', 'incidencia.id_prioridad')
            ->orderBy('created_at', 'desc')
            ->get();

        return view('modal_timeline', compact(
            'timeline',
            'referencia_pedido',
            'pedido_externo',
            'nombre_cliente',
            'rut_cliente',
            'productos',
            'estados',
            'prioridades'
        ));
    }


    public function dashboardEditCliente(Request $request)
    {
        /**------------------------------------------------------------------------
         *                           VARIABLES
         *------------------------------------------------------------------------**/
        $referencia_pedido  = $request->referencia_pedido; // PV3243
        $pedido_externo     = $request->pedido_externo; // ariztia000000086
        $dir_entrega        = $request->dir_entrega; // Puente Alto
        $nombre_cliente     = $request->nombre_cliente; // Juan Perez
        $rut_cliente        = $request->rut_cliente; // 12345678-9
        /*---------------------------- END OF VARIABLES ----------------------------*/

        return view('modal_editCliente', compact(
            'referencia_pedido',
            'pedido_externo',
            'dir_entrega',
            'nombre_cliente',
            'rut_cliente'
        ));
    }


    public function dashboardUpdateCliente(Request $request)
    {
        /**------------------------------------------------------------------------
         *                           VARIABLES
         *------------------------------------------------------------------------**/
        $referencia_pedido  = $request->form["referencia_pedido"]; // PV3243
        $dir_entrega        = $request->form["dir_entrega"]; // Puente Alto
        $nombre_cliente     = $request->form["nombre_cliente"]; // Juan Pérez
        $rut_cliente        = $request->form["rut_cliente"]; // 12345678-9
        /*---------------------------- END OF VARIABLES ----------------------------*/

        $editCliente = Partida::where('referencia_pedido', $referencia_pedido);

        $editCliente->update([
            'referencia_pedido' => $referencia_pedido,
            'dir_entrega'       => $dir_entrega,
            'nombre_cliente'    => $nombre_cliente,
            'rut_cliente'       => $rut_cliente
        ]);

        if ($editCliente) {
            return response()->json(['mensaje' => 'ok']);
        } else {
            return response()->json(['mensaje' => 'ng']);
        }
    }
}
