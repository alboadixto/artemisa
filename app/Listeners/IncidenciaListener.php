<?php

namespace App\Listeners;

use App\Notifications\IncidenciaNotification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class IncidenciaListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //si viene [canal_venta] con data [administrador] envio solo al administrador
        $column = 'canal_venta';
        if ($event->canal_venta == 'Administrador') {
            $column = 'name';
        }

        User::all()
            ->where($column, $event->canal_venta)
            ->each(function (User $user) use ($event) {
                Notification::send($user, new IncidenciaNotification($event->incidencia));
            });
    }
}
