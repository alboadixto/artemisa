@extends("layout")

@section('titulo')
    Pedidos
@endsection

@section('titulo_content_header')
    Pedidos - {{ ($canal_venta) ? $canal_venta : 'LUM' }}

{{--     <div class="col-sm-5 float-sm-right">
        <form action="{{route('exportarIncidencia')}}" method="GET">
            <div class="row">

                <div class="col-5">
                    <select class="form-control" name="empresa_nombre">
                        @if (!$canal_venta)
                            <option value="">Todas las empresas</option>
                        @endif

                        @foreach ($empresas as $key => $value)
                            <option value="{{ $value }}" {{ ( $value == $canal_venta) ? 'selected' : '' }}>
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="col-7">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>

                        <input type="text" class="form-control float-right daterange" name="daterange_filter_excel" id="daterange_filter_excel" autocomplete="off">

                        <span class="input-group-append">
                            <button type="submit" class="btn bg-gradient-success btn-sm">
                                <i class="fas fa-file-excel"></i>
                            </button>
                        </span>
                    </div>
                </div>

            </div>
        </form>
    </div> --}}
@endsection

@section("scriptsPlugins")
    <script>

        $(document).ready(function() {

            $("#card-title").text("Pedidos");

            //obtener el canal de venta del usuario que ingreso al sistema y lo envio en la url del datatable para filtrar ese valor
            var canal_venta = @json($canal_venta); //Marley Coffee

            /**------------------------------------------------------------------------
             *                           DATATABLE
             * https://yajrabox.com/docs/laravel-datatables/master
             *------------------------------------------------------------------------**/
            $("#tabla-data").DataTable({
                "language": {
                    "sProcessing":     '<i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Cargando...</div>',
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "bLengthChange": false,
                "bInfo": false,
                "ordering": false,
                "processing": true,
                "serverSide": true,
                //"order": [[ 1, "desc" ]],
                "ajax":{
                    url : "{{ route('datatable-incidencia') }}",
                    type: "GET",
                    data: {"canal_venta": canal_venta} //Marley Coffee
                },
                "columns": [
                    {data: 'btn'},
                    {data: 'canal_venta',       name: 'canal_venta'},
                    {data: 'sitio_origen',      name: 'sitio_origen'},
                    {data: 'referencia_pedido', name: 'referencia_pedido'},
                    {data: 'pedido_externo',    name: 'pedido_externo'},
                    {data: 'dir_entrega',       name: 'dir_entrega'},
                    {data: 'nombre_cliente',    name: 'nombre_cliente'},
                    {data: 'rut_cliente',       name: 'rut_cliente'},
                ]
            });
            /*---------------------------- END OF DATATABLE ----------------------------*/

        });

        /**------------------------------------------------------------------------
         *                           DATERANGE PICKER
         *------------------------------------------------------------------------**/
        const fecha_ini = "{{$fecha_ini}}";
        const fecha_fin = "{{$fecha_fin}}";
        const today     = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()); //Día actual
        $('.daterange').daterangepicker({
            "startDate" : fecha_ini,
            "endDate"   : fecha_fin,
            "maxDate"   : today,
            "locale"    : {
                "format"            : "YYYY-MM-DD",
                "separator"         : " | ",
                "applyLabel"        : "Aplicar",
                "cancelLabel"       : "Cancelar",
                "fromLabel"         : "DE",
                "toLabel"           : "HASTA",
                "customRangeLabel"  : "Custom",
                "daysOfWeek"        : [ "Dom",
                                        "Lun",
                                        "Mar",
                                        "Mie",
                                        "Jue",
                                        "Vie",
                                        "Sáb"],
                "monthNames"        : [ "Enero",
                                        "Febrero",
                                        "Marzo",
                                        "Abril",
                                        "Mayo",
                                        "Junio",
                                        "Julio",
                                        "Agosto",
                                        "Septiembre",
                                        "Octubre",
                                        "Noviembre",
                                        "Diciembre"],
                "firstDay"          : 1
            }
        })
        /*---------------------------- END OF DATERANGE PICKER ----------------------------*/

    </script>
@endsection

@section('scripts')
    <script>

        //* boton que se encuentra en incidencia/btn-datatable-incidencia.blade.php
        function mostrar_comentario_incidencia (referencia_pedido, pedido_externo, cliente_nombre, cliente_rut){

            var data = {
                            'referencia_pedido' : referencia_pedido,//PV3243
                            'pedido_externo'    : pedido_externo,   //ariztia000000086
                            'cliente_nombre'    : cliente_nombre,   //Fabiola Sepúlveda Avalos
                            'cliente_rut'       : cliente_rut,      //15028017-6
            };

            $.ajax({
                url : '{{url("incidencia/comentario")}}',
                type: 'GET',
                data: data,
                beforeSend: function(){
                    $("#card-title").text("Pedido interno: "+referencia_pedido+" | Pedido externo: "+pedido_externo+" | Cliente: "+cliente_nombre+" | RUT: "+cliente_rut);
                    //ocultar tabla de incidencia
                    $("#table-incidencia").hide('fast');
                    //mostrar comentarios de la incidencia
                    $("#comentario-incidencia").show('fast');
                },
                success: function (data){
                    $('#comentario-incidencia').html(data);
                },
                error: function (){
                },
                complete: function(){
                    //ocultar el icono de cargando...
                    $(".overlay").hide();
                }
            });
        }

        function changeVisibility() {
            $("#card-title").text("Incidencia");
            //mostrar tabla de incidencia
            $("#table-incidencia").show('fast');
            //ocultar comentarios de la incidencia
            $("#comentario-incidencia").hide('fast');
            //mostrar el icono de cargando...
            $(".overlay").show('slow');
        }

        //boton que se encuentra en IncidenciaController.php
        function btn_guardar_comentario_incidencia(){
            //const form = $("#form-comentario-incidencia");
            const form = {
                    "referencia_pedido" : $("#referencia_pedido").val(), //PV3243
                    "id_estado"         : $("#id_estado").val(), //3
                    "id_prioridad"      : $("#id_prioridad").val(), //1
                    "comentario"        : $("#comentario").val(),
                    "caso_cerrado"      : 0,
            };
            const referencia_pedido = $("#referencia_pedido").val(); //PV3243
            const pedido_externo = $("#pedido_externo").val(); //ariztia000000086
            ajax_comentario_incidencia(form, referencia_pedido, pedido_externo);
        }

        //boton que se encuentra en IncidenciaController.php
        function btn_cerrar_caso_comentario_incidencia(){
            alertify.prompt( '¿Porqué desea cerrar el caso?'
                , ''
                , function(evt, value) {
                    if(value == ''){
                        alertify.error('Ingrese el comentario')
                        return false;
                    }

                    const form = {
                        "referencia_pedido" : $("#referencia_pedido").val(), //PV3243
                        "id_estado"         : 6, // Caso cerrado
                        "id_prioridad"      : 3, // Alta
                        "comentario"        : value,
                        "caso_cerrado"      : 1,
                    };
                    const referencia_pedido = $("#referencia_pedido").val(); //PV3243
                    const pedido_externo = $("#pedido_externo").val(); //ariztia000000086
                    ajax_comentario_incidencia(form, referencia_pedido, pedido_externo);
                }
                , function() {
                    alertify.error('Cancelado')
                }
            );
        }

        //funcion que se encarga de guardar el comentario en la tabla incidencia
        function ajax_comentario_incidencia(form, referencia_pedido, pedido_externo){
            $.ajax({
                url: '{{url("incidencia/guardar_comentario")}}',
                type: 'POST',
                //data: form.serialize(),
                data: {"form": form},
                beforeSend: function(){
                    //deshabilitar el boton hasta que ingrese el comentario en la base de datos
                    $('.btn_guardar').attr("disabled", true);
                    //mostrar el icono de cargando...
                    $(".overlay").show();
                },
                success: function (respuesta){
                    if (respuesta.mensaje == "ok"){
                        alertify.success('Comentario guardado correctamente.');

                        /* //Pusher
                        var channel = pusher.subscribe('incidencia');
                        channel.bind('pusher:subscription_succeeded', function(data) {
                            alert(JSON.stringify('data'));
                        }); */

                    } else{
                        alertify.error('Comentario no pudo ser guardado.');
                    }
                },
                error: function (){
                    alertify.error('Error al ingresar comentario.');
                },
                complete: function(){
                    //habilita el boton
                    $('.btn_guardar').attr("disabled", false);
                    changeVisibility();
                    $("#tabla-data").DataTable().ajax.reload();
                }
            });

        }


    </script>
@endsection

@section('contenido')
    {{--
    @foreach ($roles as $rol)
        {{$rol['nombre']}}
    @endforeach
    --}}
    <div class="card card-secondary">

        <div class="card-header">
            <h3 class="card-title"><span id="card-title"></span></h3>
        </div>
        <!-- /.card-header -->

        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">

                    <div class="table-responsive" id='table-incidencia'>
                        <table class="table table-hover table-sm" id="tabla-data">
                            <thead>
                                <tr>
                                    <th width="60px"></th>
                                    <th>Canal Ventas</th>
                                    <th>Sitio Origen</th>
                                    <th>Ref. Pedido</th>
                                    <th>Pedido Ext.</th>
                                    <th>Dir. Entrega</th>
                                    <th>Cliente</th>
                                    <th>RUT</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div id='comentario-incidencia' style="display: none;">
                    </div>

                </div>
            </div>
        </div>
        <!-- /.card-body -->

    </div>
@endsection
