<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                {{-- Formulario editar cliente --}}

                <div class="card card-info">

                    <div class="card-header">
                        <h3 class="card-title">Cliente</h3>
                    </div>
                    <!-- /.card-header -->

                    <!-- form start -->
                    <form role="form" class="form-horizontal" id="form-edit-cliente">
                        <div class="card-body">

                            <input type="hidden" id="referencia_pedido" name="referencia_pedido" value="{{$referencia_pedido}}">
                            <input type="hidden" id="pedido_externo" name="pedido_externo" value="{{$pedido_externo}}">

                            <div class="form-group row">
                                <label for="dir_entrega" class="col-sm-2 col-form-label">Dirección</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="dir_entrega" name="dir_entrega" placeholder="Puente Alto" value="{{$dir_entrega}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="nombre_cliente" class="col-sm-2 col-form-label">Nombre</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" placeholder="Juan Pérez" value="{{$nombre_cliente}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rut_cliente" class="col-sm-2 col-form-label">RUT</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="rut_cliente" name="rut_cliente" placeholder="12345678-9" value="{{$rut_cliente}}">
                                </div>
                            </div>

                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-info float-right btn_guardar">Actualizar</button>
                        </div>
                    </form>
                    <!-- /.form start -->
                </div>

                {{-- Fin Formulario editar cliente --}}

            </div>
            <!-- /.col -->
        </div>
    </div>
    <!-- /.timeline -->
</section>
<!-- /.content -->

<script>
    /**------------------------------------------------------------------------
     *                           VALIDATE
     *------------------------------------------------------------------------**/
    $.validator.setDefaults({
        submitHandler: function () {
            btn_guardar_update_cliente();
        }
    });

    $('#form-edit-cliente').validate({
        rules: {
            dir_entrega: {
                required: true
                //email: true,
            },
            nombre_cliente: {
                required: true
            },
            rut_cliente: {
                required: true,
                minlength: 9
            },
        },
        messages: {
            dir_entrega: {
                required: "Por favor ingrese la dirección"
            },

            nombre_cliente: {
                required: "Por favor ingrese el nombre"
            },

            rut_cliente: {
                required: "Por favor ingrese el RUT",
                minlength: "RUT debe contener al menos 9 caracteres"
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    /*---------------------------- END OF VALIDATE ----------------------------*/
</script>
