<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('titulo', 'AdminLTE 3') | Artemisa</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset("libs/css/all.min.css")}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("libs/adminlte.min.css")}}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Datatables https://datatables.net/download/-->
    <link rel="stylesheet" type="text/css" href="{{asset("libs/dataTables.bootstrap4.min.css")}}" />

    <!-- bootstrap-fileinput -->
    <link rel="stylesheet" href="{{asset("libs/fileinput.min.css")}}" />

    <!-- AlertifyJS -->
    <link rel="stylesheet" href="{{asset("libs/alertify.min.css")}}" />
    <link rel="stylesheet" href="{{asset("libs/bootstrap.min.css")}}" />

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{asset("libs/daterangepicker.css")}}">

    @yield('styles')

    @livewireStyles

</head>

<body class="hold-transition sidebar-mini sidebar-collapse">

    <div class="wrapper">
        <!-- Inicio Header -->
        @include("header")
        <!-- Fin Header -->
        <!-- Inicio Aside -->
        @include("aside")
        <!-- Fin Aside -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-12">
                            <h1>@yield('titulo_content_header', 'titulo header')</h1>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    @yield('contenido')
                </div>
            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        <!-- Inicio Footer -->
        @include("footer")
        <!-- Fin Footer -->

    </div>

    <!-- jQuery -->
    <script src="{{asset("libs/jquery.min.js")}}"></script>

    <!-- Bootstrap 4 -->
    <script src="{{asset("libs/bootstrap.bundle.min.js")}}"></script>

    <!-- jquery-validation -->
    <script src="{{asset("libs/jquery.validate.min.js")}}"></script>
    <script src="{{asset("libs/additional-methods.min.js")}}"></script>

    <!-- Datatables https://datatables.net/download/-->
    <script src="{{asset("libs/jquery.dataTables.js")}}"></script>
    <script src="{{asset("libs/dataTables.bootstrap4.min.js")}}"></script>

    <!-- AdminLTE App -->
    <script src="{{asset("libs/adminlte.min.js")}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset("libs/demo.js")}}"></script>

    <!-- bootstrap-fileinput -->
    <script src="{{asset("libs/fileinput.min.js")}}"></script>
    <!-- bootstrap-fileinput / theme -->
    <script src="{{asset("libs/theme.min.js")}}"></script>
    <!-- bootstrap-fileinput / español -->
    <script src="{{asset("libs/es.js")}}"></script>

    <!-- AlertifyJS -->
    <script src="{{asset("libs/alertify.min.js")}}"></script>
    <script type="text/javascript">
        //override defaults
        alertify.defaults.theme.ok = "btn btn-primary";
        alertify.defaults.theme.cancel = "btn btn-danger";
        alertify.defaults.theme.input = "form-control";
        alertify.defaults.glossary.title = '<i class="icon fas fa-exclamation-triangle"></i> Importante!';
        alertify.defaults.glossary.ok = 'Aceptar';
        alertify.defaults.glossary.cancel = 'Cancelar';
    </script>

    <!-- date-range-picker -->
    <script src="{{asset("libs/moment.min.js")}}"></script>
    <script src="{{asset("libs/daterangepicker.js")}}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    </script>

    <!-- Pusher -->
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;

        var pusher = new Pusher('0937d57580d9a908a4ba', {
        cluster: 'us2'
        });

        var channel = pusher.subscribe('incidencia');
        channel.bind('App\\Events\\IncidenciaEvent', function(data) {
            //alert(JSON.stringify(data));
            window.livewire.emit('mensajeRecibido', data);
        });

    </script>

    @livewireScripts

    @yield("scriptsPlugins")

    @yield('scripts')

</body>

</html>
