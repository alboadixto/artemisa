<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                {{-- Productos --}}
                <div class="card card-warning collapsed-card">
                    <!-- card-header -->
                    <div class="card-header">
                        <h3 class="card-title">Productos</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->

                    <!-- card-body -->
                    <div class="card-body" style="display: none;">

                        @php
                            //variables tabla productos
                            $indice_productos       = 0;
                            $cantidad_total         = 0;
                            $precio_total           = 0;
                        @endphp

                        <div class="table-responsive">
                            <table class="table table-hover table-s text-nowrap">
                                <thead>
                                    <tr>
                                        <th width="10px">#</th>
                                        <th>Nombre</th>
                                        <th width="10px">Cantidad</th>
                                        <th width="10px">Precio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($productos as $value)
                                        @php
                                            $indice_productos++;
                                            $cantidad_total+= $value->cantidad;
                                            $precio_total+= $value->precio;
                                        @endphp

                                        <tr>
                                            <td>{{$indice_productos}}</td>
                                            <td>{{$value->nombre_mostrado}}</td>
                                            <td align="center">{{$value->cantidad}}</td>
                                            <td align="right">${{number_format($value->precio, 0, '', '.')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" align="right">Total</td>
                                        <td align="center">{{$cantidad_total}}</td>
                                        <td>${{number_format($precio_total, 0, '', '.')}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                    <!-- /.card-body -->

                </div>
                {{-- Fin Productos --}}

                {{-- Formulario de incidencias --}}
                <div class="card card-info collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Formulario Incidencias</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->

                    <div class="card-body" style="display: none;">
                        <!-- form start -->
                        <form role="form" class="form-horizontal" id="form-comentario-incidencia">
                            <div class="card-body">

                                <input type="hidden" id="referencia_pedido" name="referencia_pedido" value="{{$referencia_pedido}}">
                                <input type="hidden" id="pedido_externo" name="pedido_externo" value="{{$pedido_externo}}">
                                <input type="hidden" id="nombre_cliente" name="nombre_cliente" value="{{$nombre_cliente}}">
                                <input type="hidden" id="rut_cliente" name="rut_cliente" value="{{$rut_cliente}}">

                                <div class="form-group row">
                                    <label for="id_estado" class="col-sm-2 col-form-label">Estado</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="id_estado" id="id_estado">
                                        @foreach ($estados as $key => $value)
                                            <option value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                        </select>
                                    </div>

                                    <label for="id_prioridad" class="col-sm-2 col-form-label">Prioridad</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="id_prioridad" id="id_prioridad">
                                        @foreach ($prioridades as $key => $value)
                                            <option value="{{ $key }}">
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="comentario" class="col-sm-2 col-form-label">Comentario</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="4" id="comentario" name="comentario" placeholder="Comentario"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /.form start -->
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="button" class="btn btn-info float-right btn_guardar" onclick="btn_guardar_comentario_incidencia()">Guardar</button>
                        <button type="button" class="btn btn-danger btn_cerrar_caso" onclick="btn_cerrar_caso_comentario_incidencia()">Cerrar Caso</button>
                    </div>
                    <!-- /.card-footer -->
                </div>
                {{-- Fin Formulario de incidencias --}}

                {{-- TimeLine --}}
                <div class="timeline">
                    @foreach ($timeline as $item)
                        @php
                            $color = 'blue';
                            if($item->caso_cerrado == 1){
                                $color = 'red';
                            }
                        @endphp
                        <!-- timeline time label -->
                        <div class="time-label">
                            <span class="bg-{{$color}}">{{$item->estado_descripcion}}</span>
                        </div>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        <div>
                            <i class="fas fa-comments bg-{{$color}}"></i>
                            <div class="timeline-item">

                                {{-- <div class="ribbon-wrapper">
                                    <div class="ribbon bg-{{$item->color_prioridad}}">
                                    {{$item->prioridad_descripcion}}
                                    </div>
                                </div> --}}

                                <span class="time">
                                    <i class="fas fa-clock"></i>
                                    {{$item->created_at->diffForHumans()}}
                                </span>

                                <h3 class="timeline-header"><a href="#">{{$item->name}}</a></h3>

                                <div class="timeline-body">
                                    {{$item->comentario}}
                                </div>

                                <div class="timeline-footer">
                                    <span class="badge bg-{{$item->color_prioridad}}">{{$item->prioridad_descripcion}}</span>
                                </div>
                            </div>
                        </div>
                        <!-- END timeline item -->
                    @endforeach

                    <div>
                        <i class="fas fa-clock bg-gray"></i>
                    </div>
                </div>
                {{-- Fin Timeline --}}

            </div>
        </div>
    </div>
</section>
<!-- /.content -->
