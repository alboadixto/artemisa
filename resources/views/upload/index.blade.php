@extends("layout")

@section('titulo')
    Subir archivo
@endsection

@section('titulo_content_header')
    Subir archivo
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        // plugins subir archivos - https://plugins.krajee.com/file-input
        $('#file').fileinput({
            uploadUrl: "{{URL::to('upload/create')}}",
            allowedFileExtensions: ['xlsx','xls'],  //fileType: "any"
            //allowedFileTypes: ['image'],  // allow only images
            maxFileSize: 5000,
            maxFileCount: 1,
            //showUpload: false,
            theme: 'fas',
            language: 'es',
            browseOnZoneClick: true
        }).on('fileuploaded', function(event, previewId, index, fileId) {
            console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
            $("#tabla-data").DataTable().ajax.reload();
        }).on('fileuploaderror', function(event, data, msg) {
            console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
        }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
            console.log('File Batch Uploaded', preview, config, tags, extraData);
        });


        /* plugins datatable - https://yajrabox.com/docs/laravel-datatables/master */
        $("#tabla-data").DataTable({
            "language": {
                "sProcessing":     '<i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Cargando...</div>',
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "bLengthChange": false,
            "bInfo": false,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            "ajax": "{{ url('api/datatable-partida')}}",
            "columns": [
                {data: 'canal_venta',       name: 'canal_venta'},
                {data: 'sitio_origen',      name: 'sitio_origen'},
                {data: 'referencia_pedido', name: 'referencia_pedido'},
                {data: 'pedido_externo',    name: 'pedido_externo'},
                {data: 'nombre_mostrado',   name: 'nombre_mostrado'},
                {data: 'cantidad',          name: 'cantidad'},
                {data: 'precio',            name: 'precio'},
                {data: 'dir_entrega',       name: 'dir_entrega'},
                {data: 'nombre_cliente',    name: 'nombre_cliente'},
                {data: 'rut_cliente',       name: 'rut_cliente'},
            ]
        });
    });
</script>
@endsection

@section('contenido')
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Partida</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form role="form">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="custom-file">
                                <input id="file" name="file[]" type="file" accept=".xlsx, .xls">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-sm" id="tabla-data">
                            <thead>
                                <tr>
                                    <th>Canal Ventas</th>
                                    <th>Sitio Origen</th>
                                    <th>Ref. Pedido</th>
                                    <th>Pedido Ext.</th>
                                    <th>Nombre</th>
                                    <th>Cant.</th>
                                    <th>Precio</th>
                                    <th>Dir. Entrega</th>
                                    <th>Cliente</th>
                                    <th>RUT</th>
                                </tr>
                            </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
