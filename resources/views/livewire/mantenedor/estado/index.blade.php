@extends("layout")

@section('titulo')

    Estados

@endsection

@section('titulo_content_header')

    Mantenedor

@endsection

@section('scripts')
    <script>

        window.addEventListener('alert:confirm', event => {
            alertify.confirm(event.detail.title, event.detail.message,
            function(){
                window.livewire.emit('delete', event.detail.id);
            }
            , function(){
                alertify.error('Cancelado')
            });
        })

        Livewire.on('alert', function(message){
            alertify.notify(message, 'success', 5, function(){
                console.log('dismissed');
            });
        })

    </script>
@endsection

@section('contenido')

    @livewire('mantenedor.estados')

@endsection
