<div>
    @if ($formulario)
        @include('livewire.mantenedor.estado.create')
    @endif

    @if (!$formulario)
        <!--  Datatable -->
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">Estado</h3>

                <div class="float-sm-right">
                    <button wire:click="crear()" class="btn bg-gradient-success btn-sm"><i class="fas fa-plus-circle"></i> Nuevo</button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="table-responsive" id='table-dashboard'>
                            <table class="table table-hover table-sm" id="tabla-data">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Activo?</th>
                                        <th>Descripción</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 0;
                                    @endphp
                                    @foreach ($estados as $estado)
                                        @php
                                            $count++;
                                            $activo = $estado->is_active == 1 ? 'Si' : 'No';
                                            $color = $estado->is_active == 1 ? 'success' : 'danger';
                                        @endphp
                                        <tr>
                                            <th style="vertical-align:middle; width: 40px;">{{$count}}</th>
                                            <td style="text-align:center; vertical-align:middle; width: 50px;"><small class="badge badge-{{$color}}">{{$activo}}</small></td>
                                            <td style="vertical-align:middle; width: 450px;">{{$estado->estado_descripcion}}</td>
                                            <td style="vertical-align:middle; width: auto;">
                                                <button wire:click="edit({{$estado->id}})" class="btn btn-info" type="submit">Editar</button>
                                                <button wire:click="deleteConfirm({{$estado->id}})" class="btn btn-danger" type="submit">Borrar</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.Datatable-->
    @endif
</div>
