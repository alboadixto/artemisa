<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">Crear</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">

                <form>
                    <div class="mb-3">
                      <label for="descripcion" class="form-label">Descripción</label>
                      <input type="text" class="form-control" id="descripcion" name="descripcion" wire:model="estado_descripcion" tabindex="1">
                    </div>

                    <div class="mb-3">
                        <label for="active" class="form-label">Activo</label>
                        <select class="form-control" id="active" name="active" wire:model="is_active" tabindex="2">
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>

                    <button wire:click.prevent="cerrarFormulario()" type="submit" class="btn btn-secondary" tabindex="3">Cancelar</button>
                    <button wire:click.prevent="store()" type="submit" class="btn btn-primary" tabindex="2">Guardar</button>
                </form>

            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
