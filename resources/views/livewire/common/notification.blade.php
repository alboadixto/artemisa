<div>
    <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>

            @if ($count_unread > 0)
                <span class="badge badge-warning navbar-badge">{{ $count_unread }}</span>
            @endif
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

            @if ($count_unread > 0)
                <span class="dropdown-item dropdown-header">{{ $count_unread }} {{ $count_unread > 1 ? 'Notificaciones no leídas' : 'Notificación no leída'}}</span>

                @foreach ($unreadNotifications as $unreadNotification)
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item" wire:click="markNotification('{{$unreadNotification->id}}')">
                        <i class="fas fa-envelope mr-2"></i> {{ $unreadNotification->data['referencia_pedido'] }}
                        <span class="float-right text-muted text-sm">{{ $unreadNotification->created_at->diffForHumans() }}</span>
                    </a>
                @endforeach
            @endif

            @if ($count_read > 0)
                <span class="dropdown-item dropdown-header">{{ $count_read }} {{ $count_read > 1 ? 'Notificaciones leídas' : 'Notificación leída'}}</span>

                @foreach ($readNotifications as $readNotification)
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> {{ $readNotification->data['referencia_pedido'] }}
                        <span class="float-right text-muted text-sm">{{ $readNotification->created_at->diffForHumans() }}</span>
                    </a>
                @endforeach
            @endif

            {{-- <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a> --}}
        </div>
    </li>
</div>
