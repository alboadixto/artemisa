@extends("layout")

@section('titulo')

    Estado - Nuevo

@endsection

@section('titulo_content_header')

    Nuevo estado

@endsection

@section("scriptsPlugins")

    <script>
        $(document).ready(function() {

            $("#card-title").text("Formulario");

        });
    </script>

@endsection

@section('contenido')

    <!--  Datatable -->
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title"><span id="card-title"></span></h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">

                    <form action="{{route('estado.store')}}" method="POST">
                        @csrf
                        <div class="mb-3">
                          <label for="descripcion" class="form-label">Descripción</label>
                          <input type="text" class="form-control" id="descripcion" name="descripcion" tabindex="1" required>
                        </div>
                        <a href="{{route('estado.index')}}" class="btn btn-secondary" tabindex="3">Cancelar</a>
                        <button type="submit" class="btn btn-primary" tabindex="2">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.Datatable-->

@endsection
