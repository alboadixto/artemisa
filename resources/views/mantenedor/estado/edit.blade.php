@extends("layout")

@section('titulo')

    Estado - Editar

@endsection

@section('titulo_content_header')

    Editar estado

@endsection

@section("scriptsPlugins")

    <script>
        $(document).ready(function() {

            $("#card-title").text("Formulario");

        });
    </script>

@endsection

@section('contenido')

    <!--  Datatable -->
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title"><span id="card-title"></span></h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">

                    <form action="{{route('estado.update', $estado->id)}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="mb-3">
                          <label for="descripcion" class="form-label">Descripción</label>
                          <input type="text" class="form-control" id="descripcion" name="descripcion" tabindex="1" value="{{$estado->estado_descripcion}}">
                        </div>

                        <div class="mb-3">
                            <label for="is_active" class="form-label">Activo</label>
                            <select class="form-control" id="is_active" name="is_active" tabindex="2">
                                <option {{old('is_active',$estado->is_active)== 1 ? 'selected' : ''}}  value="1">Si</option>
                                <option {{old('is_active',$estado->is_active)== 0 ? 'selected' : ''}} value="0">No</option>
                            </select>
                        </div>

                        <a href="{{route('estado.index')}}" class="btn btn-secondary" tabindex="4">Cancelar</a>
                        <button type="submit" class="btn btn-primary" tabindex="3">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.Datatable-->

@endsection
