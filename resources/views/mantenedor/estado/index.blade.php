@extends("layout")

@section('titulo')

    Estados

@endsection

@section('titulo_content_header')

    Mantenedor

    <div class="float-sm-right">
        <a href="{{route('estado.create')}}" class="btn bg-gradient-success btn-sm"><i class="fas fa-plus-circle"></i> Nuevo</a>
    </div>

@endsection

@section("scriptsPlugins")

    <script>
        $(document).ready(function() {

            $("#card-title").text("Estado");

        });
    </script>

@endsection

@section('scripts')
    <script>

        $('.delete_estado').on('click', function (e) {
            e.preventDefault() // Don't post the form, unless confirmed
            alertify.confirm('<i class="icon fas fa-exclamation-triangle"></i> Importante!', 'Está seguro que desea eliminar el registro?',
            function(){
                $(e.target).closest('form').submit() // Post the surrounding form
            }
            , function(){
                alertify.error('Cancelado')
            });
        });

    </script>
@endsection

@section('contenido')

    <!--  Datatable -->
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title"><span id="card-title"></span></h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">

                    <div class="table-responsive" id='table-dashboard'>
                        <table class="table table-hover table-sm" id="tabla-data">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Activo?</th>
                                    <th>Descripción</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $count = 0;
                                @endphp
                                @foreach ($estados as $estado)
                                    @php
                                        $count++;
                                        //$activo = $estado->is_active == 1 ? 'Si' : 'No';
                                        $icon = 'fa-times';
                                        $color = 'danger';
                                        if($estado->is_active == 1){
                                            $icon = 'fa-check';
                                            $color = 'primary';
                                        }
                                    @endphp
                                    <tr>
                                        <th style="vertical-align:middle; width: 40px;">{{$count}}</th>
                                        <td style="text-align:center; vertical-align:middle; width: 50px;"><small class="badge badge-{{$color}}"><i class="fas {{$icon}}"></i></small></td>
                                        <td style="vertical-align:middle; width: 450px;">{{$estado->estado_descripcion}}</td>
                                        <td style="vertical-align:middle; width: auto;">
                                            <form action="{{route('estado.destroy', $estado->id)}}" method="POST">
                                                <a href="{{route('estado.edit', $estado->id)}}" class="btn btn-info">Editar</a>
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger delete_estado" type="submit">Borrar</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.Datatable-->

@endsection
