<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ingresar | Artemisa</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset("libs/css/all.min.css")}}">
  <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("libs/adminlte.min.css")}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{route('inicio')}}" class="brand-link">
      <img src="{{asset("images/artemisa_full.png")}}"
            style="max-width:100%;width:auto;height:auto;"
            alt="Artemisa - Sistema de gestión de incidencias">
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
        <div class="error-content">
            <h4><i class="fas fa-exclamation-triangle text-warning"></i> Periodo de prueba finalizado.</h4>

            <p>
              Si desea seguir utilizando nuestra plataforma por favor póngase en contacto con el administrador.
            </p>

        </div>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset("libs/jquery.min.js")}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset("libs/bootstrap.bundle.min.js")}}"></script>
<!-- AdminLTE App -->
<script src="{{asset("libs/adminlte.min.js")}}"></script>

</body>
</html>
