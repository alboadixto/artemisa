<a href="javascript:;" onclick="mostrar_timeline_incidencia('{{$referencia_pedido}}', '{{$pedido_externo}}', '{{$nombre_cliente}}', '{{$rut_cliente}}')">

        @if($caso_cerrado == 1)
            <span class="text-sm badge bg-danger">{{$estado}}</span><br>
            <small>{{$primer_estado}}</small>

            @php

                /* $fecha_comentario_santiago  = date('Y-m-d H:i:s', strtotime('+2 day', strtotime($fecha_primer_comentario)));

                $fecha_comentario_regiones  = date('Y-m-d H:i:s', strtotime('+5 day', strtotime($fecha_primer_comentario))); */

                $color_sla  = 'success';

                //* REGION METROPOLITANA
                if(intval($SLA) > 47 && $region_nombre == 'Metropolitana de Santiago'){
                    $color_sla = 'danger';

                //* DISTINTA A REGION METROPOLITANA
                }elseif(intval($SLA > 119)){
                    $color_sla = 'danger';
                }

            @endphp

            <p class="text-{{$color_sla}}"><i class="far fa-clock"></i> {{$SLA}}</p>

        @else
            <span class="text-sm badge bg-info">{{$estado}}</span><br>
            <small>{{$primer_estado}}</small>
        @endif

</a>
