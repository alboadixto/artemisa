@extends("layout")

@section('titulo')

    Dashboard

@endsection

@section('titulo_content_header')

    Dashboard - {{ ($canal_venta) ? $canal_venta : 'LUM' }}

    <div class="float-sm-right">
        <a href="{{route('exportarIncidencia', ['fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin, 'canal_venta' => $canal_venta])}}" class="btn bg-gradient-success btn-sm"><i class="fas fa-file-excel"></i> Incidencias</a>

        {{-- <form action="{{route('exportarIncidencia')}}" method="GET">
            <div class="row">

                <div class="col-5">
                    <select class="form-control" name="empresa_nombre">
                        @if (!$canal_venta)
                            <option value="">Todas las empresas</option>
                        @endif

                        @foreach ($empresas as $key => $value)
                            <option value="{{ $value }}" {{ ( $value == $canal_venta) ? 'selected' : '' }}>
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="col-7">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>

                        <input type="text" class="form-control float-right daterange" name="daterange_filter_excel" id="daterange_filter_excel" autocomplete="off">

                        <span class="input-group-append">
                            <button type="submit" class="btn bg-gradient-success btn-sm">
                                <i class="fas fa-file-excel"></i>
                            </button>
                        </span>
                    </div>
                </div>

            </div>
        </form> --}}
    </div>

@endsection

@section("scriptsPlugins")

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>

        $(document).ready(function() {

            $("#card-title").text("Incidencia");

        });

        //obtener el canal de venta del usuario que ingreso al sistema y lo envio en la url del datatable para filtrar ese valor
        var canal_venta   = @json($canal_venta); //Marley Coffee
        let valor         = 3; //Se inicializa en 3 (Todos)
        const fecha_ini   = "{{$fecha_ini}}";
        const fecha_fin   = "{{$fecha_fin}}";

        /**------------------------------------------------------------------------
         *                           DATATABLE
         * https://yajrabox.com/docs/laravel-datatables/master
         *------------------------------------------------------------------------**/
        $("#tabla-data").DataTable({
            "language": {
                "sProcessing":     '<i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Cargando...</div>',
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "bLengthChange": false,
            "bInfo": false,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            //"order": [[ 1, "desc" ]],
            "ajax":{
                url : "{{ route('datatable-dashboard') }}",
                type: "GET",
                //data: {"valor": valor}
                data: function (d) {
                        d.canal_venta   = canal_venta;
                        d.valor         = valor;
                        d.fecha_ini     = fecha_ini;
                        d.fecha_fin     = fecha_fin;
                    }
            },
            "columns": [
                {data: 'btn'},
                {data: 'canal_venta',       name: 'canal_venta'},
                {data: 'sitio_origen',      name: 'sitio_origen'},
                {data: 'referencia_pedido', name: 'referencia_pedido'},
                {data: 'pedido_externo',    name: 'pedido_externo'},
                {data: 'btn-cliente'},
            ]
        });
        /*---------------------------- END OF DATATABLE ----------------------------*/


        /**------------------------------------------------------------------------
         *                           DATERANGE PICKER
         *------------------------------------------------------------------------**/
        const today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()); //Día actual
        $('.daterange').daterangepicker({
            "startDate" : fecha_ini,
            "endDate"   : fecha_fin,
            "maxDate"   : today,
            "locale"    : {
                "format"            : "YYYY-MM-DD",
                "separator"         : " | ",
                "applyLabel"        : "Aplicar",
                "cancelLabel"       : "Cancelar",
                "fromLabel"         : "DE",
                "toLabel"           : "HASTA",
                "customRangeLabel"  : "Custom",
                "daysOfWeek"        : [ "Dom",
                                        "Lun",
                                        "Mar",
                                        "Mie",
                                        "Jue",
                                        "Vie",
                                        "Sáb"],
                "monthNames"        : [ "Enero",
                                        "Febrero",
                                        "Marzo",
                                        "Abril",
                                        "Mayo",
                                        "Junio",
                                        "Julio",
                                        "Agosto",
                                        "Septiembre",
                                        "Octubre",
                                        "Noviembre",
                                        "Diciembre"],
                "firstDay"          : 1
            }
        })
        /*---------------------------- END OF DATERANGE PICKER ----------------------------*/


        /**------------------------------------------------------------------------
         *                           GRÁFICOS
         *------------------------------------------------------------------------**/
        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function grafico_estados() {

            var chart_estados = @json($chart_estados);
            var datos_estados = [];

            chart_estados.forEach(function(entry) {
                datos_estados.push([entry.estado_descripcion,entry.cantidad]);
            });
            console.log(datos_estados);

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Estado');
            data.addColumn('number', 'Cantidad');
            /*
            data.addColumn('string', 'Month'); // Implicit domain label col. - ej: 'April'
            data.addColumn('number', 'Sales'); // Implicit series 1 data col. - ej: 1000
            data.addColumn({type:'number', role:'interval'});  // interval role col. - ej: 900
            data.addColumn({type:'number', role:'interval'});  // interval role col. - ej: 1100
            data.addColumn({type:'string', role:'annotation'}); // annotation role col. - ej: 'A'
            data.addColumn({type:'string', role:'annotationText'}); // annotationText col. - ej: 'Stolen data'
            data.addColumn({type:'boolean',role:'certainty'}); // certainty col. - ej: true
            data.addColumn('date', 'Season Start Date'); // - ej: new Date(2000, 8, 5)
            data.addColumn({type: 'string', role: 'style'}); // - ej: 'color: red' (no funciona con gráfico de tortas)
            */
            data.addRows(
                datos_estados
            );

            // Set chart options
            var options = {//'title':'Estados',
                            //'colors': ['#dc3545', '#007bff', '#28a745', '#ffc107','#20c997', '#6610f2', '#6c757d', '#fd7e14'],
                            'height':300,
                            'legend': { position: 'right' }
                        };

            // Instantiate and draw our chart, passing in some options.
            var chart_estados = new google.visualization.PieChart(document.getElementById('chart_estados'));
            chart_estados.draw(data, options);
        }

        function grafico_efectividad() {

            var chart_efectividad = @json($chart_efectividad);
            console.log('total: '+chart_efectividad.total);
            console.log('parcial: '+chart_efectividad.parcial);

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Efectividad');
            data.addColumn('number', 'Cantidad');
            /*
            data.addColumn('string', 'Month'); // Implicit domain label col. - ej: 'April'
            data.addColumn('number', 'Sales'); // Implicit series 1 data col. - ej: 1000
            data.addColumn({type:'number', role:'interval'});  // interval role col. - ej: 900
            data.addColumn({type:'number', role:'interval'});  // interval role col. - ej: 1100
            data.addColumn({type:'string', role:'annotation'}); // annotation role col. - ej: 'A'
            data.addColumn({type:'string', role:'annotationText'}); // annotationText col. - ej: 'Stolen data'
            data.addColumn({type:'boolean',role:'certainty'}); // certainty col. - ej: true
            data.addColumn('date', 'Season Start Date'); // - ej: new Date(2000, 8, 5)
            data.addColumn({type: 'string', role: 'style'}); // - ej: 'color: red' (no funciona con gráfico de tortas)
            */
            data.addRows([
                ['Total', chart_efectividad.total],
                ['Parcial', chart_efectividad.parcial],
            ]);

            // Set chart options
            var options = {//'title':'Estados',
                            //'colors': ['#dc3545', '#007bff', '#28a745', '#ffc107','#20c997', '#6610f2', '#6c757d', '#fd7e14'],
                            'height':300,
                            'legend': { position: 'right' }
                        };

            // Instantiate and draw our chart, passing in some options.
            var chart_efectividad = new google.visualization.PieChart(document.getElementById('chart_efectividad'));
            chart_efectividad.draw(data, options);
        }

        function grafico_incidencia_vs_venta() {

            // Create the data table.
            var data = google.visualization.arrayToDataTable([
                ['Month', 'Incidencias', 'Ventas'],
                @foreach($chart_incidencia_vs_venta as $item)

                    ['{{$item->mes}}', {{$item->incidencia}}, {{$item->venta}}],

                @endforeach
            ]);

            // Set chart options
            var options = {//'title':'Estados',
                            'height':300,
                            curveType: 'function',
                            'legend': { position: 'bottom' }
                        };

            // Instantiate and draw our chart, passing in some options.
            var chart_incidencia_vs_venta = new google.visualization.LineChart(document.getElementById('chart_incidencia_vs_venta'));
            chart_incidencia_vs_venta.draw(data, options);
        }

        function grafico_sla() {

            var chart_cumplimiento_sla = @json($chart_cumplimiento_sla);

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Cumplimiento');
            data.addColumn('number', 'Cantidad');

            data.addRows([
                ['Cumplido',    chart_cumplimiento_sla.cumplido_sla],
                ['No Cumplido', chart_cumplimiento_sla.no_cumplido_sla],
            ]);

            // Set chart options
            var options = {//'title':'Estados',
                            'height':300,
                            'legend': { position: 'right' }
                        };

            // Instantiate and draw our chart, passing in some options.
            var chart_sla = new google.visualization.PieChart(document.getElementById('chart_sla'));
            chart_sla.draw(data, options);
        }

        function drawChart() {
            grafico_estados();
            grafico_efectividad();
            grafico_incidencia_vs_venta();
            grafico_sla();
        }
        /*---------------------------- END OF GRÁFICOS ----------------------------*/

    </script>

@endsection

@section('scripts')

    <script>

        function mostrar_datatable(val){ //En desarrollo -> 0 | Caso cerrado -> 1 | Demorado -> 2 | Todos -> 3
            console.log(valor);
            valor = val;
            $("#tabla-data").DataTable().ajax.reload();
        }

        /* Modal timeline */
        function mostrar_timeline_incidencia (referencia_pedido, pedido_externo, nombre_cliente, rut_cliente){
            const data = {
                    "referencia_pedido" : referencia_pedido, //PV3243
                    "pedido_externo"    : pedido_externo, //ariztia000000086
                    "nombre_cliente"    : nombre_cliente, //angelo carrasco
                    "rut_cliente"       : rut_cliente, //17610656-5
            };

            $.ajax({
                url: "{{ route('dashboardTimeline') }}",
                type: 'GET',
                data: data,
                beforeSend: function(){

                    //titulo del modal timeline
                    $("#modal-title").html(`
                        <small>
                            <span class="badge badge-secondary">
                                Pedido interno: <cite>`+referencia_pedido+`</cite> -
                                Pedido externo: <cite>`+pedido_externo+`</cite>
                            </span> |
                            <span class="badge badge-secondary">
                                Cliente: <cite>`+nombre_cliente+`</cite> -
                                RUT: <cite>`+rut_cliente+`</cite>
                            </span>
                        </small>
                    `);

                    //mostrar el icono de cargando...
                    $(".overlay").show();

                    //mostrar modal timeline
                    $('#modal-xl').modal('show');

                },
                success: function (data){
                    $('#modal').html(data);
                },
                error: function (){
                },
                complete: function(){
                    //ocultar el icono de cargando...
                    $(".overlay").hide();
                }
            });
        }

        /* Modal editar cliente */
        function mostrar_edit_cliente (referencia_pedido, pedido_externo, dir_entrega, nombre_cliente, rut_cliente){
            const data = {
                    "referencia_pedido" : referencia_pedido, //PV3243
                    "pedido_externo"    : pedido_externo, //ariztia000000086
                    "dir_entrega"       : dir_entrega, //Puente Alto
                    "nombre_cliente"    : nombre_cliente, //Juan Perez
                    "rut_cliente"       : rut_cliente, //12345678-9
            };

            $.ajax({
                url: "{{ route('dashboardEditCliente') }}",
                type: 'GET',
                data: data,
                beforeSend: function(){

                    //titulo del modal timeline
                    $("#modal-title").html(`
                        <small>
                            <span class="badge badge-secondary">
                                Pedido interno: <cite>`+referencia_pedido+`</cite> -
                                Pedido externo: <cite>`+pedido_externo+`</cite>
                            </span>
                        </small>
                    `);

                    //mostrar el icono de cargando...
                    $(".overlay").show();

                    //mostrar modal
                    $('#modal-xl').modal('show');

                },
                success: function (data){
                    $('#modal').html(data);
                },
                error: function (){
                },
                complete: function(){
                    //ocultar el icono de cargando...
                    $(".overlay").hide();
                }
            });
        }

        //boton que se encuentra en modal_timeline.blade.php
        function btn_guardar_comentario_incidencia(){
            const form = {
                    "referencia_pedido" : $("#referencia_pedido").val(), //PV3243
                    "id_estado"         : $("#id_estado").val(), //3
                    "id_prioridad"      : $("#id_prioridad").val(), //1
                    "comentario"        : $("#comentario").val(),
                    "caso_cerrado"      : 0,
            };
            const referencia_pedido = $("#referencia_pedido").val(); //PV3243
            const pedido_externo    = $("#pedido_externo").val(); //ariztia000000086
            const nombre_cliente    = $("#nombre_cliente").val(); //angelo carrasco
            const rut_cliente       = $("#rut_cliente").val(); //17610656-5
            ajax_comentario_incidencia(form, referencia_pedido, pedido_externo, nombre_cliente, rut_cliente);
        }

        //boton que se encuentra en modal_timeline.blade.php
        function btn_cerrar_caso_comentario_incidencia(){
            alertify.prompt( '¿Porqué desea cerrar el caso?'
                , ''
                , function(evt, value) {
                    if(value == ''){
                        alertify.error('Ingrese el comentario')
                        return false;
                    }

                    const form = {
                        "referencia_pedido" : $("#referencia_pedido").val(), //PV3243
                        "id_estado"         : 6, // Caso cerrado
                        "id_prioridad"      : 3, // Alta
                        "comentario"        : value,
                        "caso_cerrado"      : 1,
                    };
                    const referencia_pedido = $("#referencia_pedido").val(); //PV3243
                    const pedido_externo    = $("#pedido_externo").val(); //ariztia000000086
                    const nombre_cliente    = $("#nombre_cliente").val(); //angelo carrasco
                    const rut_cliente       = $("#rut_cliente").val(); //17610656-5
                    ajax_comentario_incidencia(form, referencia_pedido, pedido_externo, nombre_cliente, rut_cliente);
                }
                , function() {
                    alertify.error('Cancelado')
                }
            );
        }

        //Se encarga de guardar el comentario en la tabla incidencia
        function ajax_comentario_incidencia(form, referencia_pedido, pedido_externo, nombre_cliente, rut_cliente){
            $.ajax({
                url: '{{url("incidencia/guardar_comentario")}}',
                type: 'POST',
                //data: form.serialize(),
                data: {"form": form},
                beforeSend: function(){
                    //deshabilitar el boton hasta que ingrese el comentario en la base de datos
                    $('.btn_guardar').attr("disabled", true);
                    //mostrar el icono de cargando...
                    $(".overlay").show();
                },
                success: function (respuesta){
                    if (respuesta.mensaje == "ok"){
                        alertify.success('Comentario guardado correctamente.');
                    } else{
                        alertify.error('Comentario no pudo ser guardado.');
                    }
                },
                error: function (){
                    alertify.error('Error al ingresar comentario.');
                },
                complete: function(){
                    //habilita el boton
                    $('.btn_guardar').attr("disabled", false);
                    mostrar_timeline_incidencia(referencia_pedido, pedido_externo, nombre_cliente, rut_cliente);
                    $("#tabla-data").DataTable().ajax.reload();
                }
            });

        }

        //boton que se encuentra en modal_editCliente.blade.php
        function btn_guardar_update_cliente(){
            const form = {
                    "referencia_pedido" : $("#referencia_pedido").val(), //PV3243
                    "dir_entrega"       : $("#dir_entrega").val(), //Puente Alto
                    "nombre_cliente"    : $("#nombre_cliente").val(), //Juan Pérez
                    "rut_cliente"       : $("#rut_cliente").val(), //12345678-9
            };
            ajax_update_cliente(form);
        }

        //Se encarga de guardar la modificación del cliente (dirección, nombre y RUT)
        function ajax_update_cliente(form){
            $.ajax({
                url: "{{ route('dashboardUpdateCliente') }}",
                type: 'POST',
                //data: form.serialize(),
                data: {"form": form},
                beforeSend: function(){
                    //deshabilitar el boton hasta que ingrese el cliente en la base de datos
                    $('.btn_guardar').attr("disabled", true);
                    //mostrar el icono de cargando...
                    $(".overlay").show();
                },
                success: function (respuesta){
                    if (respuesta.mensaje == "ok"){
                        alertify.success('Cliente modificado correctamente.');
                        //cerrar modal editar cliente
                        $('#modal-xl').modal('hide');
                    } else{
                        alertify.error('Cliente no pudo ser modificado.');
                    }
                },
                error: function (){
                    alertify.error('Error al ingresar Cliente.');
                },
                complete: function(){
                    //habilita el boton
                    $('.btn_guardar').attr("disabled", false);
                    //recarga el Datatable
                    $("#tabla-data").DataTable().ajax.reload();
                }
            });

        }

    </script>

@endsection

@section('contenido')

    <div class="row">

        {{-- FILTROS --}}
        <div class="col-sm-6">
            <div class="form-group">
                <form action="{{route('inicio')}}" method="GET">
                    <div class="row">

                        <div class="col-sm-6">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-briefcase"></i>
                                    </span>
                                </div>
                                <select class="form-control float-right" name="canal_venta">
                                    @if (!$user_canal_venta)
                                        <option value="">Canal de venta - Todos</option>
                                    @endif

                                    @foreach ($empresas as $key => $value)
                                        <option value="{{ $value }}" {{ ( $value == $canal_venta) ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="col-sm-6">

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control float-right daterange" name="daterange_filter" id="daterange_filter" autocomplete="off">

                                <span class="input-group-append">
                                    <button type="submit" class="btn btn-primary btn-flat">Ir</button>
                                </span>
                            </div>

                        </div>

                    </div>
                </form>
            </div>
        </div>
        {{-- END FILTROS --}}


        {{-- TOP INCIDENCIAS --}}
        <div class="col-lg-12 col-12">
            <div class="card card-warning collapsed-card">

                <div class="card-header">
                    <h3 class="card-title">Top Incidencias</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>

                <div class="card-body" style="display: none;">
                    <div class="row">

                        {{-- TABLA INCIDENCIAS --}}
                        <div class="col-sm-12">
                            <table class="table table-hover table-sm">
                                <thead>
                                    <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Estado</th>
                                    <th class="text-center">Cantidad</th>
                                    <th style="width: 90px">Monto ($)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i              = 0;
                                        $cantidad_total = 0;
                                        $precio_total   = 0;
                                    @endphp
                                    @foreach ($top_incidencias as $item)
                                        @php
                                            $i++;
                                            $cantidad_total += $item->cantidad;
                                            $precio_total += $item->precio;
                                        @endphp
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$item->estado_descripcion}}</td>
                                            <td class="text-center">{{$item->cantidad}}</td>
                                            <td class="text-right">${{number_format($item->precio, 0, '', '.')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" class="text-right">Total</td>
                                        <td class="text-center">{{$cantidad_total}}</td>
                                        <td class="text-right">${{number_format($precio_total, 0, '', '.')}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        {{-- END TABLA INCIDENCIAS --}}

                    </div>
                </div>

            </div>
        </div>
        {{-- END TOP INCIDENCIAS --}}


        {{-- SMALL BOXES --}}
        <div class="col-lg-3 col-12">
            <!-- En desarrollo -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$count_desarrollo}}</h3>

                    <p>EN DESARROLLO</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-stopwatch"></i>
                </div>
                <a href="#" class="small-box-footer" onclick="mostrar_datatable(0)">Mostrar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-12">
            <!-- Caso cerrado -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{$count_caso_cerrado}}</h3>

                    <p>CASO CERRADO</p>
                </div>
                <div class="icon">
                    <i class="ion ion-lock-combination"></i>
                </div>
                <a href="#" class="small-box-footer" onclick="mostrar_datatable(1)">Mostrar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-12">
            <!-- Todos -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{$count_demorado}}</h3>

                    <p>DEMORADO</p>
                </div>
                <div class="icon">
                    <i class="ion ion-clock"></i>
                </div>
                <a href="#" class="small-box-footer" onclick="mostrar_datatable(2)">Mostrar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-12">
            <!-- Todos -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$count_todos}}</h3>

                    <p>TODOS</p>
                </div>
                <div class="icon">
                    <i class="ion ion-clipboard"></i>
                </div>
                <a href="#" class="small-box-footer" onclick="mostrar_datatable(3)">Mostrar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        {{-- END SMALL BOXES --}}


        {{-- GRÁFICOS --}}
        <div class="col-lg-6 col-12">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  Estados
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <!--Div that will hold the pie chart-->
                    <div id="chart_estados"></div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-lg-6 col-12">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  Efectividad
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <!--Div that will hold the pie chart-->
                    <div id="chart_efectividad"></div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-lg-6 col-12">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  Incidencias VS Ventas
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <!--Div that will hold the pie chart-->
                    <div id="chart_incidencia_vs_venta"></div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-lg-6 col-12">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  SLA
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                </div>
                </div>
                <div class="card-body">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <!--Div that will hold the pie chart-->
                    <div id="chart_sla"></div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        {{-- END GRÁFICOS --}}

    </div>

    <!--  Datatable -->
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title"><span id="card-title"></span></h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">

                    <div class="table-responsive" id='table-dashboard'>
                        <table class="table table-hover table-sm" id="tabla-data">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Canal Venta</th>
                                    <th>Sitio Origen</th>
                                    <th>Ref. Pedido</th>
                                    <th>Pedido Ext.</th>
                                    <th>Cliente</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.Datatable-->

    <!-- Modal -->
    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">

                <div class="overlay">
                    <i class="fas fa-2x fa-sync fa-spin"></i>
                    <div class="text-bold pt-2">Cargando...</div>
                </div>

                <div class="modal-header">
                    <h4 class="modal-title"><span id="modal-title"></span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal">
                    <!-- modal contenido -->
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal-->

@endsection
