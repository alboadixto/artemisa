<a href="javascript:;" onclick="mostrar_edit_cliente('{{$referencia_pedido}}', '{{$pedido_externo}}', '{{$dir_entrega}}', '{{$nombre_cliente}}', '{{$rut_cliente}}')">

    <div class="form-group row">
            <span class="col-sm-4"><i class="fas fa-map-marker-alt"></i> {{$dir_entrega}} <br>
            <small>{{$region_nombre}}</small></span>
            <span class="col-sm-5"><i class="fas fa-user"></i> {{$nombre_cliente}}</span>
            <span class="col-sm-3"><i class="fas fa-id-card"></i> {{$rut_cliente}}</span>
    </div>
</a>
