  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('inicio')}}" class="brand-link">
      <img src="{{asset("images/AdminLTELogo.png")}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Artemisa</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset("images/user2-160x160.jpg")}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{session()->get('usuario_nombre') ?? 'Invitado'}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="{{route('uploadFile')}}" class="{{ request()->routeIs('uploadFile') ? 'nav-link active' : 'nav-link' }}">
                    <i class="nav-icon fas fa-file-upload"></i>
                    <p>Subir archivo</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('incidencia')}}" class="{{ request()->routeIs('incidencia') ? 'nav-link active' : 'nav-link' }}">
                    <i class="nav-icon fas fa-clipboard-list"></i>
                    @php
                        //* usuario_canal_venta se setea en app/User.php
                        $canal_venta = session()->get('usuario_canal_venta'); // Marley Coffee
                    @endphp
                    <p>Pedidos - {{ ( $canal_venta) ? $canal_venta : 'LUM' }}</p>
                </a>
            </li>
            <li class="nav-header">Mantenedor</li>
            <li class="nav-item">
                <a href="{{route('estado.index')}}" class="{{ request()->routeIs('estados') ? 'nav-link active' : 'nav-link' }}">
                    <i class="nav-icon far fa-edit"></i>
                    <p>Estado</p>
                </a>
            </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
