<?php

use App\Events\IncidenciaEvent;
use App\Http\Controllers\IncidenciaController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\UploadController;
use App\Http\Livewire\Mantenedor\Estados;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth']], function () {

    /**------------------------------------------------------------------------------------------------
     *                                         Inicio
     *------------------------------------------------------------------------------------------------**/
    //Route::get('/', 'InicioController@index')->name('inicio');
    Route::get('/', [InicioController::class, 'index'])->name('inicio');
    Route::get('dashboard/timeline', [InicioController::class, 'dashboardTimeline'])->name('dashboardTimeline');
    Route::get('dashboard/edit_cliente', [InicioController::class, 'dashboardEditCliente'])->name('dashboardEditCliente');
    Route::post('dashboard/update_cliente', [InicioController::class, 'dashboardUpdateCliente'])->name('dashboardUpdateCliente');

    /**------------------------------------------------------------------------------------------------
     *                                         Subir excel
     *------------------------------------------------------------------------------------------------**/
    Route::get('upload/index', [UploadController::class, 'index'])->name('uploadFile');
    Route::post('upload/create', [UploadController::class, 'create'])->name('uploadFileCreate');

    /**------------------------------------------------------------------------------------------------
     *                                         Incidencia
     *------------------------------------------------------------------------------------------------**/
    Route::get('incidencia/index', [IncidenciaController::class, 'index'])->name('incidencia');
    Route::get('incidencia/comentario', [IncidenciaController::class, 'mostrarComentario'])->name('mostrarComentario');
    Route::post('incidencia/guardar_comentario', [IncidenciaController::class, 'guardarComentario'])->name('guardarComentario');
    Route::get('incidencia/exportar', [IncidenciaController::class, 'exportarIncidencia'])->name('exportarIncidencia');

    /**------------------------------------------------------------------------------------------------
     *                                         Mantenedores
     *------------------------------------------------------------------------------------------------**/
    /* Route::get('estados/index', 'EstadoController@index')->name('estado.index');
    Route::get('estados/create', 'EstadoController@create')->name('estado.create');
    Route::post('estados/store', 'EstadoController@store')->name('estado.store');
    Route::get('estados/{id}/edit', 'EstadoController@edit')->name('estado.edit');
    Route::put('estados/{id}', 'EstadoController@update')->name('estado.update');
    Route::delete('estados/{id}', 'EstadoController@destroy')->name('estado.destroy'); */

    //Livewire
    //Route::get('/estados', Estados::class)->name('estados');
    Route::get('/estados', function () {
        return view('livewire.mantenedor.estado.index');
    })->name('estado.index');
});

Auth::routes();
