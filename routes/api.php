<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//subir archivo excel en tabla partida (resources/views/upload/index.blade.php)
Route::get('datatable-partida', function () {
    $model = App\Models\Partida::query()
        ->whereRaw('date(created_at) = curdate()');
    return datatables()
        ->eloquent($model)
        ->toJson();
});

// (resources/views/incidencia/index.blade.php)
Route::get('datatable-incidencia', function (Request $request) {
    $canal_venta = $request->canal_venta; //Marley Coffee ->si el usuario que ingreso al sistema tiene canal de venta, solo se mostraran esos registros

    $model = App\Models\Partida::query()
        ->select(
            'partida.canal_venta',
            'partida.sitio_origen',
            'partida.referencia_pedido',
            'partida.pedido_externo',
            'partida.dir_entrega',
            'partida.nombre_cliente',
            'partida.rut_cliente'
        )
        ->whereNotIn('partida.referencia_pedido', function ($query) {
            $query->select('referencia_pedido')
                ->from('incidencia');
        })
        ->when($canal_venta, function ($query, $canal_venta) {
            return $query->where('partida.canal_venta', $canal_venta);
        })
        ->groupBy(
            'partida.canal_venta',
            'partida.sitio_origen',
            'partida.referencia_pedido',
            'partida.pedido_externo',
            'partida.dir_entrega',
            'partida.nombre_cliente',
            'partida.rut_cliente'
        )
        ->orderBy('partida.referencia_pedido', 'desc');
    return datatables()
        ->eloquent($model)
        ->addColumn('btn', 'incidencia.btn-datatable-incidencia')
        ->rawColumns(['btn'])
        ->toJson();
})->name('datatable-incidencia');

// (resources/views/inicio.blade.php) - Dashboard
Route::get('datatable-dashboard', function (Request $request) {
    $canal_venta = $request->canal_venta; //Marley Coffee ->si el usuario que ingreso al sistema tiene canal de venta, solo se mostraran esos registros

    $valor      = $request->valor;
    $fecha_ini  = $request->fecha_ini;
    $fecha_fin  = $request->fecha_fin;

    $filtro = "";
    if ($valor == 0) {
        $filtro = "where caso_cerrado = 0";
    }
    if ($valor == 1) {
        $filtro = "where caso_cerrado = 1";
    }
    if ($valor == 2) {
        $filtro = "where caso_cerrado = 0
            and (SELECT HOUR(
            (TIMEDIFF(i2.fecha,
                (SELECT
                        i3.created_at
                    FROM
                        incidencia i3
                    WHERE
                        i3.referencia_pedido = i2.referencia_pedido
                    ORDER BY i3.id desc
                    LIMIT 1,1))))
            ) > 24 ";
    }


    $model = App\Models\Partida::query()
        ->select(
            DB::raw(
                '
                (select e.estado_descripcion from estado e where e.id = i.id_estado) as estado
                , i.caso_cerrado
                , (select created_at
                    from incidencia
                    where incidencia.referencia_pedido = partida.referencia_pedido
                    order by 1 desc limit 1) as fecha_comentario
                , (select (TIMEDIFF( fecha_comentario, (select min(i.created_at)
                                                        from incidencia i
                                                        where i.referencia_pedido = partida.referencia_pedido)))) as SLA
                , (SELECT
                    region_nombre
                    FROM comunas
                    INNER JOIN provincias ON
                        comunas.provincia_id = provincias.provincia_id
                    INNER JOIN regiones ON
                        provincias.region_id = regiones.region_id
                    WHERE comunas.comuna_nombre LIKE partida.dir_entrega) as region_nombre
                , (SELECT e.estado_descripcion
                    FROM incidencia i
                    INNER JOIN estado e
                    ON i.id_estado = e.id
                    WHERE i.referencia_pedido = partida.referencia_pedido
                    ORDER BY i.id ASC
                    LIMIT 1) AS primer_estado'
            ),
            'partida.canal_venta',
            'partida.sitio_origen',
            'partida.referencia_pedido',
            'partida.pedido_externo',
            'partida.dir_entrega',
            'partida.nombre_cliente',
            'partida.rut_cliente'
        )
        ->join(DB::raw('(select i.referencia_pedido, i.created_at ,i.caso_cerrado, i.id_estado
                                                from incidencia i
                                                inner join
                                                    (
                                                        select i.referencia_pedido,
                                                        max(i.created_at) fecha
                                                        from incidencia i
                                                        where i.created_at BETWEEN "' . $fecha_ini . '" and "' . $fecha_fin . '"
                                                        group by i.referencia_pedido) i2
                                                on i.created_at = i2.fecha
                                                ' . $filtro . ') i'), 'partida.referencia_pedido', 'i.referencia_pedido')
        ->when($canal_venta, function ($query, $canal_venta) {
            return $query->where('partida.canal_venta', $canal_venta);
        })
        ->groupBy(
            'partida.canal_venta',
            'partida.sitio_origen',
            'partida.referencia_pedido',
            'partida.pedido_externo',
            'partida.dir_entrega',
            'partida.nombre_cliente',
            'partida.rut_cliente',
            'estado',
            'i.caso_cerrado'
        )
        ->orderBy('fecha_comentario', 'desc')
        ->orderBy('partida.referencia_pedido', 'desc');
    //->toSql();
    //dd($model);
    return datatables()
        ->eloquent($model)
        ->addColumn('btn', 'dashboard.btn-datatable-dashboard')
        ->addColumn('btn-cliente', 'dashboard.btn-datatable-cliente')
        ->rawColumns(['btn', 'btn-cliente'])
        ->toJson();
})->name('datatable-dashboard');

Route::apiResource('users', 'UserController');
